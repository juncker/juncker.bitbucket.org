# Add audio

Add audio module.
You can add your audio to all document or to current slides.



## Note

CSS styles are important to have a good layout.

## How to use

create new instance of class AddAudio

Enter parameters in init method of your instance

```
/*  call init function of AddAudio instance      */
/*  Set one music to current slides   */

    audio.init({
            slidesAudio: [                                          // enter audio files to current slides
                {
                    container: '.second_slide',                     // set container name
                    sources:[                                       // enter your sources like .mp3, .ogg, .webm
                        {
                            src: 'Pharrell_Williams.mp3',           // src of your audio
                            type: "audio/mpeg"                      // web type of your audio
                        }
                    ]
                },{
                    container: '.third_slide',
                    sources:[
                        {
                            src: './audio/Linkin_Park.mp3',
                            type: "audio/mpeg"
                        }
                    ]
                },{
                    container: '.fifth_slide',
                    sources:[
                        {
                            src: './audio/Mind_Vortex.mp3',
                            type: "audio/mpeg"
                        }
                    ]
                },
            ],
            audioIcon: {                                            // set parameters for your audio button
                imageSrc: './img/units-icons.png',                  // button image source
                className: 'audio-button'                           // set class name
            },
            playButtonUrl: './img/play_button_icon.png',            // play button in main pop up window
            silentInnerClassName: 'silence-html',                   // class name to style text in main pop up window
            silentInnerHTML: 'Tap button to play music or ',        // text in main pop up window
            silenceButtonClassName:'silence-button',                // class name to style text 'silence' in main pop up window
            silenceButtonHTML:'silence'                             // text in 'silence' main pop up window
        });


/*  Set one music to document   */
        audio.init({
                globalAudio: {                                          // add music to all document
                    sources:[                                           // enter your sources like .mp3, .ogg, .webm
                        {
                            src: './audio/Armin_van_buuren.mp3',        // src of your audio
                            type: "audio/mpeg"                          // web type of your audio
                        }
                    ],
                    id: 'new-global-audio'                              // give identification name
                },

                audioIcon: {                                            // set parameters for your audio button
                    imageSrc: './img/units-icons.png',                  // button image source
                    className: 'audio-button'                           // set class name
                },
                playButtonUrl: './img/play_button_icon.png',            // play button in main pop up window
                silentInnerClassName: 'silence-html',                   // class name to style text in main pop up window
                silentInnerHTML: 'Tap button to play music or ',        // text in main pop up window
                silenceButtonClassName:'silence-button',                // class name to style text 'silence' in main pop up window
                silenceButtonHTML:'silence'                             // text in 'silence' main pop up window
            });

/*  this is default settings that I have used in my example, you can use your own   */
```