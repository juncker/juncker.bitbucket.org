
function AddArrow() {
    var _this;

    function css(elem,css){
        for (var j in css) {
            elem.style[j] = css[j];
        }
    }

    return {
        init: function (opts) {

            _this = this;

            _this.opts = opts;

            this.render(opts);

            document.addEventListener('touchmove', function() {

                _this.onTouchstart(_this);
            });
            document.addEventListener('touchend', function() {
                _this.onTouchend(_this);
            });
/*
            document.addEventListener('arrowHide', function() {

                _this.onTouchstart(_this);
            });
            document.addEventListener('arrowShow', function() {
                _this.onTouchend(_this);
            });
*/

            document.addEventListener('hideElementsEvent', _this.onTouchstart );
            document.addEventListener('showElementsEvent', _this.onTouchend );
        },

        render: function (opts) {
            var container = document.createElement(opts.elemName),
                img = document.createElement(opts.imageTagName);

            this.container = container;
            container.className = opts.elemClassName;
            img.style.backgroundImage = opts.arrowImgSrc;
            container.appendChild(img);
            css(container, opts.elemStyle);
            css(img, opts.imageStyle);
            opts.container ? opts.container.appendChild(container) : document.body.appendChild(container);
            this.container = container;
            this.img = img;



        },

        onTouchstart: function(_this) {
            _this.container.style.display = 'none';
        },

        onTouchend: function(_this) {
            _this.container.style.display = 'block';
        },

        remove: function () {
//            _this.opts.container ? _this.opts.container.removeChild(_this.container) : document.body.removeChild(_this.container);
            _this.img.style.backgroundImage = '';
            _this.container.style.display = 'none';
            document.removeEventListener('hideElementsEvent', _this.onTouchstart );
            document.removeEventListener('showElementsEvent', _this.onTouchend );

        }
    }
}
