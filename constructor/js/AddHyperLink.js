
function AddHyperLink() {

    function css(elem,css){
        for (var j in css) {
            if ( j == 'width' ) {
                elem.style[j] = css[j]*window.innerWidth/960 + 'px';
            } else {
                elem.style[j] = css[j];
            }
        }
    }

    function setAttributes(elem, attributes) {

        for ( var prop in attributes ) {
            if ( prop != 'target') {
                elem.setAttribute( prop, attributes[prop]);
            }
        }
    }

    return {
        init: function(opts) {
            _this.render(opts);

        },

        render: function(opts) {
            var div = document.createElement('div'),
                wrapper_div = document.createElement('div'),
                a = document.createElement('a'),
                fragment = document.createDocumentFragment();

            wrapper_div.className = opts.linkWrapperClassName;

            setAttributes( a, opts.linkAttributes);

            a.innerHTML = opts.linkTitle;

            div.appendChild(a);
            fragment.appendChild(div);

            var container = opts.container;

            css( div, opts.style );
            wrapper_div.appendChild(div);

            document.body.appendChild(wrapper_div);

//            console.log( a.getBoundingClientRect().width, window.innerWidth/640 );

//            div.style.height = div.getBoundingClientRect().height * window.innerHeight/960 + 'px';
//            console.log(opts.top , window.innerHeight / 960);

            wrapper_div.style.top = opts.top + 'px';
            div.style.left = opts.left /** window.innerWidth / 640*/ /* * window.innerWidth/640 */+ 'px';
            div.style.width = a.getBoundingClientRect().width /* * window.innerWidth/640 */+ 'px';

            container.appendChild(wrapper_div);

//            setTimeout(function(){


            if (opts.linkAttributes.target) {
                a.addEventListener('touchend', function(e){
                    e.preventDefault();
                    e.stopPropagation();

                    var win = window.open(opts.linkAttributes.href, opts.linkAttributes.target);
                    win.focus();
                })
            }
        }
    }
}
