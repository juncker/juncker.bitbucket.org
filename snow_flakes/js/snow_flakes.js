function SnowFlakes(options) {

    var images = [],
        counter = 0;

    return {
        options: {
            flakes : [],
            flakeCount: 2000,
            mX: -100,
            mY: -100
        },
        init: function(opts) {
            this.options = opts;

            if (opts.img) {
                opts.img.forEach(function(src){
                    var img = new Image();
                    img.src = src;
                    images.push(img);
                });
            }
            this.render(opts);
        },

        render: function(opts){

            var canvas = document.createElement('canvas'),
                context = canvas.getContext("2d");

            this.canvas = canvas;
            this.context = context;

            document.body.appendChild(canvas);

            canvas.setAttribute('width', window.innerWidth );
            canvas.setAttribute('height', window.innerHeight );
            canvas.style.position = 'absolute';
            canvas.style.left = '0px';
            canvas.style.top = '0px';
            canvas.style.marginRight = '1px';
            canvas.style.pointerEvents = 'none';

            canvas.width = window.innerWidth;
            canvas.height = window.innerHeight;

            for (var i = 0; i < this.options.flakeCount; i++) {
                var x = Math.floor(Math.random() * canvas.width),
                    y = Math.floor(Math.random() * canvas.height),
                    size = (Math.random() * 3) + 2,
                    speed = (Math.random() * 1) + 0.5,
                    opacity = (Math.random() * 0.5) + 0.3;

                this.options.flakes.push({
                    speed: speed,
                    velY: speed,
                    velX: 0,
                    x: x,
                    y: y,
                    size: size,
                    stepSize: (Math.random()) / 30,
                    step: 0,
                    angle: 180,
                    opacity: opacity
                });
            }
            this.snow(this);

        },
        snow: function () {

            var _this = this;
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            for (var i = 0; i < this.options.flakeCount; i++) {
                this.drawFlake(this.options.flakes[i]);
            }
            requestAnimationFrame(function(){
                _this.snow();
            });
        },
        drawFlake: function(flake) {
            var x = this.options.mX,
                y = this.options.mY,
                minDist = 150,
                x2 = flake.x,
                y2 = flake.y;

            var dist = Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y)),
                dx = x2 - x,
                dy = y2 - y;

            if (dist < minDist) {
                var force = minDist / (dist * dist),
                    xcomp = (x - x2) / dist,
                    ycomp = (y - y2) / dist,
                    deltaV = force / 2;

                flake.velX -= deltaV * xcomp;
                flake.velY -= deltaV * ycomp;

            } else {
                flake.velX *= .98;
                if (flake.velY <= flake.speed) {
                    flake.velY = flake.speed
                }
                flake.velX += Math.cos(flake.step += .05) * flake.stepSize;
            }

            this.context.fillStyle = "rgba(255,255,255," + flake.opacity + ")";
            flake.y += flake.velY;
            flake.x += flake.velX;

            if (flake.y >= this.canvas.height || flake.y <= 0) {
                this.reset(flake);
            }


            if (flake.x >= this.canvas.width || flake.x <= 0) {
                this.reset(flake);
            }

            this.context.beginPath();
            var _this = this;
            if ( images.length > 0 ) {

                    if ( !images[counter] ) counter = 0;
                    _this.context.drawImage(images[counter], flake.x, flake.y, flake.size, flake.size);
                    counter++;

            } else {

                this.context.arc(flake.x, flake.y, flake.size, 0, Math.PI * 2);

            }
            this.context.fill();
        },
        reset: function(flake) {
            flake.x = Math.floor(Math.random() * this.canvas.width);
            flake.y = 0;
            flake.size = (Math.random() * 20) + 2;
            flake.speed = (Math.random() * 2) + 0.5;
            flake.velY = flake.speed;
            flake.velX = 0;
            flake.opacity = (Math.random() * 0.5) + 0.3;
        }
    }
}
