function Progressline(){
    return {
        opts: {},
        slides: [],
        init: function(opts) {
            this.render(opts);
        },
        render: function(opts){
            var progressWrapper = document.createElement('div'),
                div = document.createElement('div'),
                slides = document.querySelectorAll(opts.slide);

            this.opts = opts;
            div.className = 'progress-line';
            progressWrapper.appendChild(div);
            document.body.appendChild(progressWrapper);
            this.progressLine = div;

            for ( var i = 0; i < ( slides.length ); i++ ) {
                this.slides.push(slides[i]);
            }


            this.progressWidth(opts);
        },
        progressWidth: function(opts){

            var currnetSlide = document.querySelector(this.opts.activeSlide),
                current = 0;

            for ( var i = 0; i < ( this.slides.length ); i++ ) {

                if ( this.slides[i] === currnetSlide ) {

                    current = i;

                    if ( i == (this.slides.length - 1) ) {
                        current = 1;
                    }
                    if ( i == 0 ) {
                        current = this.slides.length;
                    }

                    this.progressLine.style.width = current / ( this.slides.length - 2 ) * 100 +'%';

                }
            }
        }
    }
}