$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

//ON & OFF left side bar
$(document).ready(function(){
    $(".nav-toggle-btn").click(function(){
        $(".navigation-img-wrp").fadeToggle(200);

    });
});

//ON & OFF TOOLTIP
$(".on-off-tooltip").click(function(){
    $('li').toggleClass('off');
})

//delete images multi select
$(".toggle-wrp").click(function(){
    $('.toggle-wrp i.checked').not(this).removeClass('checked');
    $(this).toggleClass('checked');
})

//selected option in select
var sel = document.getElementById('select_music-style');
sel.addEventListener('click', function(el){
    var options = this.children;
    for(var i=0; i < this.childElementCount; i++){
        options[i].style.color = 'gray';
    }
    var selected = this.children[this.selectedIndex];
    selected.style.color = 'red';
}, false);




