
function AddHyperLink() {

    function css(elem,css){
        for (var j in css) {
            elem.style[j] = css[j];
        }
    }

    function setAttributes(elem, attributes) {

        for ( var prop in attributes ) {
            if ( prop != 'target') {
                elem.setAttribute( prop, attributes[prop]);
            }
        }
    }

    return {
        init: function(opts) {
            this.render(opts);
        },

        render: function(opts) {
            var div = document.createElement('div'),
                a = document.createElement('a'),
                fragment = document.createDocumentFragment();

            div.className = opts.linkWrapperClassName;

            setAttributes( a, opts.linkAttributes);

            a.innerHTML = opts.linkTitle;

            div.appendChild(a);
            fragment.appendChild(div);

            var container = (opts.container);

            css( div, opts.style );

            container.appendChild(fragment);

            if (opts.linkAttributes.target) {
                a.addEventListener('touchend', function(e){
                    e.preventDefault();
                    e.stopPropagation();

                    var win = window.open(opts.linkAttributes.href, opts.linkAttributes.target);
                    win.focus();
                })
            }
        }
    }
}
