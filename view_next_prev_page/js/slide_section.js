function Slider(opts){
    document.querySelector('.container').style.width = window.innerWidth+"px";
    document.querySelector('.wrapper>section').style.width = window.innerWidth+"px";
    document.querySelector('.wrapper>section').style.height = window.innerHeight+"px";

    var mySwiper = new Swiper( opts.container,{
        mode: 'vertical',
        speed: opts.speed,
        slideClass: opts.slideClass,
        slideActiveClass: 'sw-slide-active',
        slideVisibleClass: 'sw-slide-visible',
        slideDuplicateClass: 'sw-slide-duplicate',
        wrapperClass: opts.wrapperClass,
        loop: true,
        loopAdditionalSlides: 0,
        progress:true,
        onProgressChange: function(swiper){
            for (var i = 0; i < swiper.slides.length; i++){
                var slide = swiper.slides[i];
                var progress = slide.progress;
                 if ( slide.classList.contains('sw-slide-active') ) {
                     swiper.setTransform(slide,'translate3d(0px,0,'+(-Math.abs(progress*1500))+'px)');
                 } else {
                 swiper.setTransform(slide,'scale(1)');
                 }
            }
        },
        onTouchStart:function(swiper){
            for (var i = 0; i < swiper.slides.length; i++){
                swiper.setTransition(swiper.slides[i], opts.durartion);
            }
        }
    })
}






