# Slide Effect

Custom effect for slider.


## Note

Do not use in your container elements with other classes or element you don't want to slide

## How to use

create new instance of class Slider

Enter parameters in object:
```
var effect = new SliderEffect({             // create new instance
    container: '.sw-container',             // css selector for container slides container
    wrapperClass: 'sw-wrapper',             // css selector for wrapper
    slideClass: 'sw-slide',                 // class name of your slides. Without dot
    speed: 300,                             // speed of slider
    transitionDuration: 0                   // transition duration
});
```