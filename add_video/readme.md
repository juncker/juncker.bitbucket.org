# Add video

Add text module.
You can add your text with header to the slides or other containers.



## Note

CSS styles are important to have a good layout.

## How to use

create new instance of class AddText

Enter parameters in init method of your instance

```
/*  call init function of AddText instance      */
    video.init({
        container: '.second_slide',                     // container name where text will be added
        elemName: 'new-video',                          // name of element with your video
        sources: [                                      // sources of your video in different formats
            {
                src: "./video/Aston_Martin.webm",       // src of your video
                type: "video/webm"                      // type of your video
            },{
                src: "./video/Aston_Martin.flv",
                type: "video/flv"
            },{
                src: "./video/Aston_Martin.mp4",
                type: "video/mp4"
            },
        ],
        videoAttr: {                                    // set attribute for your video tag
            width: 400,
            height: 400,
            controls: 'controls',
            poster: './img/video_thumb.png'


        },
        style: {                             //css style of your container
            position: 'absolute',
            width: '100%',
            height: '50%',
            background: '#000',
            top: 0,
            left: 0

        }
    });
/*  this is default styles that I have used in my example, you can use your own   */
```