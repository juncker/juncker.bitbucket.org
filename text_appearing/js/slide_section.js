var TextEffects = function(options) {

    function injector(t, splitter, klass, after) {

        var a = t.text().split(splitter), inject = '';
        if (a.length) {
            $(a).each(function(i, item) {
                inject += '<span class="'+klass+(i+1)+'">'+item+'</span>'+after;
            });
            t.innerHTML = null;
            t.innerHTML = inject;
        }
    }


    return {
        init: function(opts) {

            var _this = this;

            _this.render(opts);

            var els = document.getElementsByTagName('*');
            for(var i=0 ; i<els.length ; i++){
                els[i].addEventListener("addClassToActive", function(e) {
                    _this.addClass(e, _this, opts);
                    _this.textAnimation(opts);

                });
            }
        },
        container: {},
        render: function(opts){
            var _this = this,
                wrapper = document.createElement('div'),
                div = document.createElement('div'),
                img = document.createElement('img'),
                fragment = document.createDocumentFragment();

            this.container = document.querySelector(opts.container);

            wrapper.className = 'text-appearing-wrapper';

            div.className = 'text-appearing '/*+opts.animationName*/;

//            injector(opts.innerHTML, '', 'char', '');
            var words = opts.innerHTML.split(' ');

            words.forEach(function(elem,i){
                var span = document.createElement('span');

                var letters = elem.split('');

                letters.forEach(function(elem,j){
                    var letter = document.createElement('span');
                    letter.innerHTML = elem;

                    letter.classList.add('char'+j);

                    span.appendChild(letter);

                });
                span.classList.add('word'+i);
                span.innerHTML += ' ';
                div.appendChild(span);
            });

            wrapper.appendChild(div);
            fragment.appendChild(wrapper);

            this.container.appendChild(fragment);

        },
        addClass: function (e) {

            var elem = e.target;
            if ( elem.classList.contains('slide-active-animation') ) return;
            elem.classList.add('slide-active-animation');



        },
        textAnimation: function(opts) {
            var _this = this;
            var counter = 0;

            var animationInterval = setInterval(function(){

                if ( !opts.effectsSequence[counter] ) clearInterval(animationInterval);
                _this.animateLetters(opts.effectsSequence[counter]);
                counter++;
            }, 3000)
        },
        animateLetters: function (effect) {

            var els = this.container.querySelectorAll('span span'),
                param = 0;

            var interval = setInterval(function(){

                els[param].classList.add('animated');
                els[param].classList.add(effect);

                (function(param){
                    setTimeout(function(){
                        els[param].classList.remove('animated');
                        els[param].classList.remove(effect);

                    }, 2000);
                })(param);

                param++;

                if ( !els[param] ) {
                    clearInterval(interval);
                }

            }, 100)

        }
    }
}
