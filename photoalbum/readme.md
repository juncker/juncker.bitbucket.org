# Add Photoalbum

Add text module.
You can add your text with header to the slides or other containers.



## Note

Do not use photoalbun in first or last slide of your collection

## How to use

create new instance of class PhotoAlbum

Enter parameters in init method of your instance

```
/*  create new instance of PhotoAlbum   */

    var photoalbum = new PhotoAlbum();

/*  call init function of PhotoAlbum instance      */


    photoalbum.init({
        container: '.nineth_slide',                 // name of container where you want to add your photoalbum
        images : [                                  // array of URLs of your pictures
            './img/53b5100d79b6542829.jpg',
            './img/53b5101cdc64f42829.jpg',
            './img/53b5102728df142829.jpg',
            './img/53b510319b83d42829.jpg',
            './img/53b5100d79b6542829.jpg',
            './img/53b5101cdc64f42829.jpg',
            './img/53b5102728df142829.jpg',
            './img/53b510319b83d42829.jpg'
        ]
    });

/*  this is default styles that I have used in my example, you can use your own   */
```