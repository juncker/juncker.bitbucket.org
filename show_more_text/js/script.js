function showMoreText(opts) {

    var array = document.querySelectorAll('.bd-wrap .smallTxt-arrow');

    for (var i = 0; i < array.length; i++) {
        array[i].addEventListener('touchend', function () {
            this.classList.contains('z-toggle') ? slideUp(this) : slideDown(this);
        })
    }

    function slideUp(elem) {
        elem.classList.remove('z-toggle');
        recursive(elem, function (elem) {
            elem.querySelector('.smallTxt-detail.j-detail').classList.remove('action');
            elem.querySelector('.smallTxt-detail.j-detail').classList.remove('z-show');
        });
    }

    function slideDown(elem) {
        elem.classList.add('z-toggle');
        recursive(elem, function (elem) {

            var action_array = document.querySelector('.smallTxt-detail.j-detail .action');
            if (action_array) {
                for (var i = 0; i < action_array.length; i++) {
                    action_array[i].classList.remove('active');
                    action_array[i].classList.remove('z-show');

                    action_array[i].style.height = '0px';
                }
            }


            elem.style.height = 86 + elem.querySelector('.smallTxt-detail.j-detail p').getBoundingClientRect().height+ 'px';

            elem.querySelector('.smallTxt-detail.j-detail').classList.add('action');
            elem.querySelector('.smallTxt-detail.j-detail').classList.add('z-show');
            elem.querySelector('.smallTxt-detail.j-detail').style.height = elem.querySelector('.smallTxt-detail.j-detail p').getBoundingClientRect().height+'px';

            console.log(elem.querySelector('.smallTxt-detail.j-detail p').getBoundingClientRect().height)
        });
    }

    function recursive(elem, func) {
        if (elem.parentNode.classList.contains('bd-wrap')) {
            if (func) {
                func(elem.parentNode);
            }
            return elem.parentNode;
        } else {
            recursive(elem.parentNode, func)
        }

    }

    var primary_height = document.querySelectorAll('.bd-wrap.j-txt');
    for (var i = 0; i < primary_height.length; i++) {
        primary_height[i].style.height = 86 + 'px';
    }
}

var show_more_text = new showMoreText();

