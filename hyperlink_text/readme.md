# Slider

Add text module.
You can add your text with header to the slides or other containers.



## Note

CSS styles are important to have a good layout.

## How to use

create new instance of class AddText

Enter parameters in init method of your instance

```
/*  Create  new  AddHyperLink instance  */

var link = new AddHyperLink();

/*  call init function of AddText instance      */

link.init({
    container: '.third_slide',                      // name of container where you want to add your photoalbum
    linkAttributes: {                               // all attributes off link you need
        href: 'http://www.5.cn',
        target: '_blanck'
    },
    linkTitle: 'Magazine',                          // text in link button
    linkWrapperClassName: 'link-for-magazine'       // class name to style button
});

/*  this is default styles that I have used in my example, you can use your own   */
```