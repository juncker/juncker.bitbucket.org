var pages = [];

pages = [
    {
        name: 'first',
        className: '.first_slide',
        preview: {
            id: '',
            url: ''
        },
        effects: [
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
                }
            },
        ]
    },
    {
        name: 'second',
        className: '.second_slide',
        effects: [
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
                }
            },
            {
                name: 'AddVideo',
                options: {
                    container: '.second_slide',         //container name where text will be added
                    elemName: 'new-video',                 //name of element with your text
                    sources: [
                        {
                            src: "http://wegazine.head.rhino.nixsolutions.com/video/Vanquish_in_Motion.mp4",
                            type: "video/mp4"
                        },{
                            src: "http://wegazine.head.rhino.nixsolutions.com/video/Vanquish_in_Motion.flv",
                            type: "video/flv"
                        },{
                            src: "http://wegazine.head.rhino.nixsolutions.com/video/Vanquish_in_Motion.webm",
                            type: "video/webm"
                        }
                    ],
                    videoAttr: {
                        width: Math.round(document.querySelector('.second_slide').getBoundingClientRect().width),
                        height: Math.round(document.querySelector('.second_slide').getBoundingClientRect().height/2),
                        controls: 'controls',
                        poster: './img/video_thumb.png',
                        style: 'width:'+Math.round(document.querySelector('.second_slide').getBoundingClientRect().width)+'px;' +
                            'height:'+Math.round(document.querySelector('.second_slide').getBoundingClientRect().height/2)+'px;'
                    },
                    style: {                            //css style of your container
                        position: 'absolute',
                        background: '#000000',
                        width: Math.round(document.querySelector('.second_slide').getBoundingClientRect().width)+'px',
                        height: Math.round(document.querySelector('.second_slide').getBoundingClientRect().height/2)+'px',

                        top: '55px',
                        left: 0

                    }
                }
            },
            {
                name: 'Smudge',
                options: {
                    container: '.second_slide',
                    smudge_material_src: './img/transparent_cover_bg_03@2x.png',       // URL of your image
                    duration: 1000,                                         // duration of disappearing
                    lineWidth: 30,                                          // width of smudge line
                    field_area: 0.5                                         // area have left to disappear
                }
            }
        ]
    },
    {
        name: 'third',
        className: '.third_slide',
        preview: {
            id: '',
            url: ''
        },
        effects: [
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
                }
            },

            {
                name: 'AddHyperLink',
                options: {
                    container: '.third_slide',
                    linkAttributes: {
                        href: 'http://www.5.cn',
                        target: '_blank'
                    },
                    linkTitle: 'Magazine',
                    linkWrapperClassName: 'link-for-magazine'
                }
            },
            {
                name: 'PhotoAlbum',
                options: {
                    container: '.third_slide',
                    images : [
                        './img/53b5100d79b6542829.jpg',
                        './img/53b5101cdc64f42829.jpg',
                        './img/53b5102728df142829.jpg',
                        './img/53b510319b83d42829.jpg',
                        './img/53b5100d79b6542829.jpg',
                        './img/53b5101cdc64f42829.jpg',
                        './img/53b5102728df142829.jpg',
                        './img/53b510319b83d42829.jpg'
                    ]
                }
            }
        ]
    },
    {
        name: 'forth',
        className: '.forth_slide',
        preview: {
            id: '',
            url: ''
        },
        effects: [
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
                }
            },

            {
                name: 'AddHyperLink',
                options: {
                    container: '.forth_slide',
                    linkAttributes: {
                        href: 'http://www.5.cn',
                        target: '_blank'
                    },
                    linkTitle: 'Magazine',
                    linkWrapperClassName: 'link-for-magazine'
                }
            }
        ]
    },
    {
        name: 'fifth',
        className: '.fifth_slide',
        preview: {
            id: '',
            url: ''
        },
        effects: [
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
                }
            },

            {
                name: 'AddText',
                options: {
                    container: '.fifth_slide',
                    elemName: 'unique',
                    setVector: 'right',

                    headingTagName: 'p',
                    driftCourse: 'up',              // up, down
                    heading: 'Heading',
                    headingClassName: 'left',

                    rows: 1,
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.',
                    style: {
                        position: 'absolute',
                        width: '400px',
                        margin: '10px',
                        bottom: '80px',
                        right: '80px'
                    },
                    textWidth: 300,

                    buttonStyle : {
                        right : '20px'
                    }
                }
            },
            {
                name: 'AddText',
                options: {
                    container: '.fifth_slide',
                    elemName: 'unique',                 //name of element with your text
                    setVector: 'left',                  //appearing vector ( left, right )
                    headingTagName: 'p',                //name of element with your text
                    driftCourse: 'down',                // drift vector of hidden text ( up, down )
                    heading: 'Very long heading',                 // your heading text
                    headingClassName: 'right',
                    rows: 1,
                    text: 'lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet ',
                    style: {
                        position: 'absolute',
                        width: '400px',
                        margin: '10px',
                        top: '50px',
                        left: '50px'
                    },
                    textWidth: 300,
                    buttonStyle : {                     //css style of your switch button
                        left : '20px'

                    }
                }
            }
        ]
    },
    {
        name: 'sixth',
        className: '.sixth_slide',
        preview: {
            id: '',
            url: ''
        },
        effects: [
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
                }
            },

            {
                name: 'EnlargeByCreator',
                options: {
                    container: '.sixth_slide',
                    elem:'.sixth_slide .m-img',
                    pointX: 0.78,
                    pointY: 0.89,
                    duration: 10,                        //  seconds
                    effect: 'ease',
                    imgScale: 3
                }
            }
        ]
    },
    {
        name: 'seventh',
        className: '.seventh_slide',
        preview: {
            id: '',
            url: ''
        },
        effects: [
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
                }
            },

            {
                name: 'AddText',
                options: {
                    container: '.seventh_slide',
                    elemName: 'unique',                 //name of element with your text
                    setVector: 'left',                  //appearing vector ( left, right )
                    headingTagName: 'p',                //name of element with your text
                    driftCourse: 'up',                // drift vector of hidden text ( up, down )
                    heading: 'Heading',                 // your heading text
                    headingClassName: 'right',
                    rows: 1,
                    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.',
                    style: {
                        position: 'absolute',
                        width: '400px',
                        margin: '10px',
                        bottom: '80px',
                        right: '80px'
                    },
                    textWidth: 400,
                    buttonStyle : {                     //css style of your switch button
                        left : '20px'

                    }
                }
            },
            {
                name: 'AddText',
                options: {
                    container: '.seventh_slide',
                    elemName: 'unique',
                    setVector: 'right',

                    headingTagName: 'p',
                    driftCourse: 'down',              // up, down
                    heading: 'Head',
                    headingClassName: 'left',
                    rows: 1,
                    text: 'lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet ',
                    style: {
                        position: 'absolute',
                        width: '400px',
                        margin: '10px',
                        top: '50px',
                        left: '50px'
                    },
                    textWidth: 450,

                    buttonStyle : {
                        right : '20px'
                    }
                }
            },
            {
                name: 'Enlarge',
                options: {
                    container: '.seventh_slide',
                    elem:'.seventh_slide .m-img',
                    duration: 0.5,                        //  seconds
                    effect: 'linear',
                    imgScale: 2,
                    innerHTML: "Let's be together"
                }
            }
        ]
    },
    {
        name: 'eighth',
        className: '.eighth_slide',
        preview: {
            id: '',
            url: ''
        },
        effects: [
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
                }
            },

            {
                name: 'AddText',
                options: {
                    container: '.eighth_slide',
                    elemName: 'unique',                 //name of element with your text
                    setVector: 'left',                  //appearing vector ( left, right )
                    headingTagName: 'p',                //name of element with your text
                    driftCourse: 'down',                // drift vector of hidden text ( up, down )
                    heading: 'Heading',                 // your heading text
                    headingClassName: 'right',
                    rows: 1,
                    text: 'lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet ',
                    style: {
                        position: 'absolute',
                        width: '400px',
                        margin: '10px',
                        top: '50px',
                        left: '50px'
                    },
                    textWidth: 400,
                    buttonStyle : {                     //css style of your switch button
                        left : '20px'

                    }
                }
            },
            {
                name: 'ObjectRolling',
                options: {
                    container: '.eighth_slide',
                    rollingImgSrc: './img/girl.png',
                    PopupImgSrc: './img/53b51697bc856.jpg',
                    linkWrapperClassName: 'link-for-magazine'
                }
            },
            {
                name: 'AddHyperLink',
                options: {
                    container: '.eighth_slide',
                    linkAttributes: {
                        href: 'http://www.5.cn',
                        target: '_blank'
                    },
                    linkTitle: 'Magazine',
                    linkWrapperClassName: 'link-for-magazine'
                }
            }
        ]
    },
    {
        name: 'ninth',
        className: '.ninth_slide',
        preview: {
            id: '',
            url: ''
        },
        effects: [
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
                }
            },

            {
                name: 'PhotoAlbum',
                options: {
                    container: '.ninth_slide',
                    images : [
                        './img/53b5100d79b6542829.jpg',
                        './img/53b5101cdc64f42829.jpg',
                        './img/53b5102728df142829.jpg',
                        './img/53b510319b83d42829.jpg',
                        './img/53b5100d79b6542829.jpg',
                        './img/53b5101cdc64f42829.jpg',
                        './img/53b5102728df142829.jpg',
                        './img/53b510319b83d42829.jpg'
                    ]
                }
            }
        ]
    },
    {
        name: 'tenth',
        className: '.tenth_slide',
        preview: {
            id: '',
            url: ''
        },
        effects: [
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
                }
            },
        ]
    },
    {
        name: 'eleventh',
        className: '.eleventh_slide',
        preview: {
            id: '',
            url: ''
        },
        effects: [
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
                }
            },

            {
                name: 'AddText',
                options: {
                    container: '.eleventh_slide',
                    elemName: 'unique',
                    setVector: 'right',

                    headingTagName: 'p',
                    driftCourse: 'down',              // up, down
                    heading: 'Heading',
                    headingClassName: 'left',
                    rows: 1,
                    text: 'lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet ',
                    style: {
                        position: 'absolute',
                        width: '400px',
                        margin: '10px',
                        top: '50px',
                        left: '50px',
                        background: 'rgba(0,0,0,0.7)',
                        color: '#fff'
                    },
                    textWidth: 300,

                    buttonStyle : {
                        right : '20px'
                    }
                }
            },
            {
                name: 'AddHyperLink',
                options: {
                    container: '.eleventh_slide',
                    linkAttributes: {
                        href: 'http://www.5.cn',
                        target: '_blank'
                    },
                    linkTitle: 'Magazine',
                    linkWrapperClassName: 'link-for-magazine'
                }
            },
            {
                name: 'TurnAroundEffect',
                options: {
                    container: '.eleventh_slide',
                    elem: '.imgbox',
                    url_data: ["./img/carroundeffect/52e24d0285a5516282.jpg",
                        "./img/carroundeffect/52e24d02bea7816282.jpg",
                        "./img/carroundeffect/52e24d02cfd0616282.jpg",
                        "./img/carroundeffect/52e24d02df8ba16282.jpg",
                        "./img/carroundeffect/52e24d02f074016282.jpg",
                        "./img/carroundeffect/52e24d030b93616282.jpg",
                        "./img/carroundeffect/52e24d031e1ea16282.jpg",
                        "./img/carroundeffect/52e24d033ccd516282.jpg",
                        "./img/carroundeffect/52e24d034d98a16282.jpg",
                        "./img/carroundeffect/52e24d035dc3516282.jpg",
                        "./img/carroundeffect/52e24d036de6a16282.jpg",
                        "./img/carroundeffect/52e24d037fa3c16282.jpg",
                        "./img/carroundeffect/52e24d039076716282.jpg",
                        "./img/carroundeffect/52e24d03af15216282.jpg",
                        "./img/carroundeffect/52e24d03bf3fd16282.jpg",
                        "./img/carroundeffect/52e24d03cf78816282.jpg",
                        "./img/carroundeffect/52e24d03e0c5016282.jpg",
                        "./img/carroundeffect/52e24d040ca7016282.jpg",
                        "./img/carroundeffect/52e24d041ddae16282.jpg",
                        "./img/carroundeffect/52e24d042d0ca16282.jpg",
                        "./img/carroundeffect/52e24d043ce8d16282.jpg",
                        "./img/carroundeffect/52e24d044da9316282.jpg",
                        "./img/carroundeffect/52e24d045d2cb16282.jpg",
                        "./img/carroundeffect/52e24d046e40616282.jpg",
                        "./img/carroundeffect/52e24d047d8db16282.jpg",
                        "./img/carroundeffect/52e24d048f02c16282.jpg",
                        "./img/carroundeffect/52e24d049e81e16282.jpg",
                        "./img/carroundeffect/52e24d04aef5716282.jpg",
                        "./img/carroundeffect/52e24d04bf20d16282.jpg",
                        "./img/carroundeffect/52e24d04cf1b516282.jpg",
                        "./img/carroundeffect/52e24d04df7d616282.jpg",
                        "./img/carroundeffect/52e24d04efea216282.jpg",
                        "./img/carroundeffect/52e24d050cceb16282.jpg",
                        "./img/carroundeffect/52e24d051c69116282.jpg",
                        "./img/carroundeffect/52e24d052c88b16282.jpg"
                    ]
                }
            }
        ]
    },
    {
        name: 'twelfth',
        className: '.twelfth_slide',
        preview: {
            id: '',
            url: ''
        },
        effects: [
            {
                name: 'Smudge',
                options: {
                    container: '.twelfth_slide',
                    smudge_material_src: './img/transparent.png',       // URL of your image
                    duration: 1000,                                         // duration of disappearing
                    lineWidth: 30,                                          // width of smudge line
                    field_area: 0.5                                         // area have left to disappear
                }
            },
            {
                name: 'Cut',
                options: {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    original_image:{
                        id: '',
                        url: ''
                    },
                    cropped_image: {
                        id: '',
                        url: ''
                    }
               }
            }
        ]
    }
];

/*
*
*
*       End of slides
*
* */



var magazine = {
    name: 'name',
    orientation: 'portrait',
    pages: [],
    effects: [{
        name: '',
        options: ''
    },{},{}]             // may be music effect and some more нада подумать
}


    var audio = new AddAudio();
    //      call init function of AddText instance
    audio.init({
//        container: 'body',
        container: '.sw-container',

        globalAudio: {
        sources:[
        {
        src: 'http://wegazine.head.rhino.nixsolutions.com/Armin_van_buuren.mp3',
        type: "audio/mpeg"
        }
        ],
        id: 'new-global-audio'
        },
        audioIcon: {
        imageSrc: './img/units-icons.png',
        className: 'audio-button'
        },
        playButtonUrl: './img/play_button_icon.png',
        silentInnerClassName: 'silence-html',
        silentInnerHTML: 'Tap button to play music or ',
        silenceButtonClassName:'silence-button',
        silenceButtonHTML:'silence'
        });

    /*  Create new instance of Smudge & set parameters  */

    var smudge = new Smudge({
        smudge_material_src: './img/cover_bg_03@2x.jpg',       // URL of your image
        duration: 1000,                                         // duration of disappearing
        lineWidth: 30,                                          // width of smudge line
        field_area: 0.5                                         // area have left to disappear
        });


    var progressline = new Progressline();
    progressline.init({
        slide: '.sw-slide',
        activeSlide: '.sw-slide-active'
        });

    var pagecounter = new PageCounter();
    pagecounter.init({
        slide: '.sw-slide',
        activeSlide: '.sw-slide-active',
        container: '.sw-left-sidebar'
        });

    var arrow = new AddArrow();
    arrow.init({
        container: '.sw-container',
        elemName: 'div',
        elemStyle: {
        display: 'block'
        },
        imageTagName: 'div',
        imageStyle: {},
        elemClassName: 'arrow',
        arrowImgSrc:'url(./img/css_sprite01.png)'
        });

