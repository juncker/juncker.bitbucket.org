function PageCounter(){
    return {
        opts: {},
        slides: [],
        init: function(opts) {
            this.render(opts);
        },
        render: function(opts){
            var progressWrapper = document.createElement('div'),
                div = document.createElement('div'),
                pageQuantitySpan = document.createElement('span'),
                pageConterSpan = document.createElement('span'),
                slides = document.querySelectorAll(opts.slide);

            this.opts = opts;
            div.className = 'page-number-counter';
            progressWrapper.appendChild(div);

            opts.container ?
            document.querySelector(opts.container).insertBefore(progressWrapper, document.querySelector(opts.container).firstChild) :
            document.body.appendChild(progressWrapper);

            this.pageQuantitySpan = pageQuantitySpan;
            this.pageConterSpan = pageConterSpan;
            this.progressLine = div;

            div.appendChild(pageConterSpan);
            div.appendChild(pageQuantitySpan);



            for ( var i = 0; i < ( slides.length ); i++ ) {
                this.slides.push(slides[i]);
            }


            this.pageNumber(opts);
        },
        pageNumber: function(opts){

            var currnetSlide = document.querySelector(this.opts.activeSlide),
                current = 0;

            for ( var i = 0; i < ( this.slides.length ); i++ ) {

                if ( this.slides[i] === currnetSlide ) {

                    current = i+1;

/*                    if ( i == (this.slides.length - 1) ) {
                        current = 1;
                    }*/
//                    if ( i == 0 ) {
//                        current = this.slides.length - 2;
//                    }
                    this.pageConterSpan.innerHTML = current + ' / ';
                    this.pageQuantitySpan.innerHTML = this.slides.length ;
                }
            }
        }
    }
}