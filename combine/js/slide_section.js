var CubeSlider = function(opts){
    var _this;

    var slideChanged = document.createEvent('Event');
    slideChanged.initEvent('slideChanged');

    return {
        mouse: {
            start : {
                x: 0,
                y: 0
            },
            end: {
                x: 0,
                y: 0
            }
        },
        opts: {},
        wrapper: document.querySelector('body'),
        init: function (opts) {
            _this = this;
            this.opts = opts;
            setTimeout(function(){
                this.wrapper = document.querySelector(opts.wrapper);
//                console.log(document.querySelector(opts.wrapper));
                var slides = this.wrapper.querySelectorAll(opts.slide);

//console.log(opts.slide)
//console.log(slides)




                slides[0].classList.add('active');
                        slides[0].classList.add('slide-active-animation');

                document.addEventListener('touchstart', _this.setStartCords);
                document.addEventListener('touchmove', _this.onMouseMove);
                document.addEventListener('touchend', _this.swipeSlides);
            },100)
        },
        onMouseMove: function (e) {
            e.preventDefault();
            e.touches ||  e.originalEvent ? e = e.touches[0] : null;
            _this.mouse.end.x = e.pageX || e.clientX;
            _this.mouse.end.y = e.pageY || e.clientY;

            _this.progressCnahge();
        },
        progressCnahge: function () {
            var elem = _this.wrapper.querySelector('.active');
            var nextSibling = elem.nextElementSibling ? elem.nextElementSibling : elem.parentNode.children[0];
            var prevSibling = elem.previousElementSibling ? elem.previousElementSibling : elem.parentNode.children[elem.parentNode.children.length-1];


            if ( _this.mouse.start.y - _this.mouse.end.y > 0) {
//                _this.next();
                elem.style.webkitTransformOrigin = '50% 0%';
                elem.style.webkitTransform = 'scale('+(Math.abs(1-(window.innerHeight - nextSibling.getBoundingClientRect().top)/window.innerHeight))+')';

//                console.log(1-(window.innerHeight - nextSibling.getBoundingClientRect().top)/window.innerHeight);

            } else {
//                _this.prev();
                elem.style.webkitTransformOrigin = '50% 100%';
                elem.style.webkitTransform = 'scale('+(Math.abs(1-(window.innerHeight - prevSibling.getBoundingClientRect().top)/window.innerHeight))+')';

//                console.log((1-((window.innerHeight - prevSibling.getBoundingClientRect().top)/window.innerHeight)));

            }

            nextSibling.classList.add('next');

            prevSibling.classList.add('prev');

            elem.style.webkitTransition  = 'none';
//            elem.style.webkitTransform = 'scale('+(1 - Math.abs(window.innerHeight - ))+') ';

            nextSibling.style.webkitTransition  = 'none';
            prevSibling.style.webkitTransition  = 'none';

            nextSibling.style.webkitTransform  = 'translate3d(0,'+((window.innerHeight)+Math.round(_this.mouse.end.y - _this.mouse.start.y)) + 'px,0)';
            prevSibling.style.webkitTransform  = 'translate3d(0,'+(-window.innerHeight+Math.round(_this.mouse.end.y - _this.mouse.start.y)) + 'px,0)';


        },
        setStartCords: function (e) {
            pagecounter.pageNumber();
            progressline.progressWidth();
            delete _this.mouse.end.y;

            e.touches ||  e.originalEvent ? e = e.touches[0] : null;
            _this.mouse.start.x = e.pageX;
            _this.mouse.start.y = e.pageY;
        },

        swipeSlides: function (e) {

            enlarge_by_creator.enlargeCancel(enlarge_by_creator);
            pagecounter.pageNumber();
            progressline.progressWidth();

            if ( _this.mouse.end.y === 0 ) _this.mouse.end.y = _this.mouse.start.y;
//            console.log('touchend', _this.mouse.start.y, _this.mouse.end.y, _this.opts.slideParameter)

            if ( _this.mouse.start.y - _this.mouse.end.y > (_this.opts.slideParameter || 100) ) {
//                console.log('next')

                _this.next();
                enlarge_by_creator.enlarge(enlarge_by_creator);
            } else if ( _this.mouse.start.y - _this.mouse.end.y < (-_this.opts.slideParameter || -100) ) {
//                console.log('prev')
                enlarge_by_creator.enlarge(enlarge_by_creator);
                _this.prev();
            } else {

                var elem = _this.wrapper.querySelector('.active');
                var prevSibling = elem.previousElementSibling ? elem.previousElementSibling : elem.parentNode.children[elem.parentNode.children.length-1];
                var nextSibling = elem.nextElementSibling ? elem.nextElementSibling : elem.parentNode.children[0];
                if (prevSibling.classList.contains('prev')) prevSibling.classList.remove('prev');
                if (nextSibling.classList.contains('next')) nextSibling.classList.remove('next');
                elem.classList.add('show');
                if (elem.classList.contains('show')) elem.classList.remove('show');
                prevSibling.style.webkitTransform = 'translate3d(0,0,0)';
                nextSibling.style.webkitTransform = 'translate3d(0,0,0)';
                elem.style.webkitTransform = 'translate3d(0,0,0)';
//                console.log('obnulateSbros')

            }

        },
        next: function () {
//            document.removeEventListener('touchstart', _this.setStartCords);
//            document.removeEventListener('touchend', _this.swipeSlides);

            var elems = _this.wrapper.querySelectorAll('.active');
            var elem = elems[0];
            for ( var i = 1; i < elems.length; i++ ) {
                if ( elems[i].classList.contains('active') ) elems[i].classList.remove('active');
            }
            var prevSibling = elem.previousElementSibling ? elem.previousElementSibling : elem.parentNode.children[elem.parentNode.children.length-1];
            var nextSibling = elem.nextElementSibling ? elem.nextElementSibling : elem.parentNode.children[0];
            elem.style.webkitTransition  = 'all 0.3s linear';
            prevSibling.style.webkitTransition  = 'all 0.3s linear';
            nextSibling.style.webkitTransition  = 'all 0.3s linear';

            prevSibling.style.webkitTransform = '';
            nextSibling.style.webkitTransform = '';
            elem.style.top = 0;

            elem.style.webkitTransform = 'scale(0)';
//            elem.style.webkitTransform = 'translate3d(0px, -'+window.innerHeight+'px, 0px)';

            if (prevSibling.classList.contains('prev')) prevSibling.classList.remove('prev');
//            if (nextSibling.classList.contains('next')) nextSibling.classList.remove('next');
//            if (elem.classList.contains('show')) elem.classList.remove('show');

//            console.log('obnulateNext')


            elem.classList.add('up');



            setTimeout(function(){
                nextSibling.style.top = 0;
                nextSibling.style.webkitTransform = 'translate3d(0px, 0px, 0px)';

                nextSibling.classList.remove('up');
                nextSibling.classList.remove('next');
                elem.classList.remove('active');
                elem.classList.remove('up');

                nextSibling.classList.add('active');
                if ( !nextSibling.classList.contains('slide-active-animation') ) {
                    nextSibling.classList.add('slide-active-animation');
                }
//                pagecounter.pageNumber();
//                progressline.progressWidth();
                document.dispatchEvent(slideChanged);
                enlarge_by_creator.enlargeCancel(enlarge_by_creator);

                console.log('next')
            }, 600);
        },
        prev: function () {
//            document.removeEventListener('touchstart', _this.setStartCords);
//            document.removeEventListener('touchend', _this.swipeSlides);

            var elems = _this.wrapper.querySelectorAll('.active');
            var elem = elems[0];
            for ( var i = 1; i < elems.length; i++ ) {
                if ( elems[i].classList.contains('active') ) elems[i].classList.remove('active');
            }
            var prevSibling = elem.previousElementSibling ? elem.previousElementSibling : elem.parentNode.children[elem.parentNode.children.length-1];

            var nextSibling = elem.nextElementSibling ? elem.nextElementSibling : elem.parentNode.children[0];
            elem.style.webkitTransition  = 'all 0.3s linear';
            prevSibling.style.webkitTransition  = 'all 0.3s linear';
            nextSibling.style.webkitTransition  = 'all 0.3s linear';

            prevSibling.style.webkitTransform = '';
            nextSibling.style.webkitTransform = '';
            elem.style.top = 0;

            elem.style.webkitTransform = 'scale(0)';
//            elem.style.webkitTransform = 'translate3d(0px, '+window.innerHeight+'px, 0px)';

//            if (prevSibling.classList.contains('prev')) prevSibling.classList.remove('prev');
            if (nextSibling.classList.contains('next')) nextSibling.classList.remove('next');
//            if (elem.classList.contains('show')) elem.classList.remove('show');


//            console.log('obnulatePrev')

            prevSibling.classList.add('prev');
            setTimeout(function(){
                elem.classList.add('down');
                prevSibling.classList.add('down');
            }, 0);


            setTimeout(function(){
                prevSibling.style.top = 0;
                prevSibling.style.webkitTransform = 'translate3d(0px, 0px, 0px)';

                prevSibling.classList.remove('down');
                prevSibling.classList.remove('prev');
                elem.classList.remove('active');

                elem.classList.remove('down');

                prevSibling.classList.add('active');

                if ( !prevSibling.classList.contains('slide-active-animation') ) {
                    prevSibling.classList.add('slide-active-animation');
                }

//                pagecounter.pageNumber();
//                progressline.progressWidth();
                document.dispatchEvent(slideChanged)
                enlarge_by_creator.enlargeCancel(enlarge_by_creator);

                console.log('prev')
            }, 600);
        }
    };
};

var cube_slider = new CubeSlider();
cube_slider.init({
    container: '.nav-slide',
    wrapper: '.wrapper',
    slide: '.sw-slide',
    rotate: 'horizontal',
    slideParameter: 100
});