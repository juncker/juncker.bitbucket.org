(function(){
    var verticalSwiperNestedSwContainer = new Swiper('.sw-coverer',{
        mode: 'horizontal',
        speed: 700,
        slideClass: 'sw-slide',
        slideActiveClass: 'sw-slide-active',
        slideVisibleClass: 'sw-slide-visible',
        slideDuplicateClass: 'sw-slide-duplicate',
        wrapperClass: 'wrapper',
        mousewheelControl : true,
        pagination: '.sw-container-pagination',
        paginationClickable: true,
        autoplay: true,
        /*  autoplay_interval my own parametr - I have customized swiper */
        autoplay_interval: 1500,
        autoplayDisableOnInteraction: false,

    });
})();

(function(){

    var slider = new DownSlider({
        duration: 200,
        effect: 'linear',
        activeClass: '.active',
        right_btn_id: '#share',
        cancel_btn: '.share-cancel-btn',
        share_box: '.share_box'

    });

    function DownSlider(opts) {
        var elems = document.querySelectorAll('.ui-btn-inner');
        for ( var i = 0; i < elems.length; i++) {
            elems[i].addEventListener('mousedown', function() {
                this.classList.add(opts.activeClass.match(/\w+/));
            })
            elems[i].addEventListener('mouseup', function() {
                this.classList.remove(opts.activeClass.match(/\w+/));
            })
        }
        var start_point = ( document.querySelector(opts.share_box).getBoundingClientRect().top - document.querySelector(opts.share_box).getBoundingClientRect().bottom )

        document.querySelector(opts.right_btn_id).addEventListener('click', function(){
            animate({
                delay: 10,
                duration: opts.duration,
                delta: eval(opts.effect),
                step: function (delta) {
                    document.querySelector(opts.share_box).style.display = 'block';
                    document.querySelector(opts.share_box).style.bottom = delta * ( 0 - start_point )+ start_point+'px';

                }
            })
        });

        document.querySelector(opts.cancel_btn).addEventListener('click', function(){
            var start_point = ( document.querySelector(opts.share_box).getBoundingClientRect().top - document.querySelector(opts.share_box).getBoundingClientRect().bottom )
            animate({
                delay: 10,
                duration: opts.duration,
                delta: quad(bounce),
                step: function (delta) {
                    document.querySelector(opts.share_box).style.bottom = delta * ( start_point - 0 )+ 0+'px';
                    setTimeout(function(){
                        document.querySelector(opts.share_box).style.display = 'none';

                    }, this.duration)
                }
            })

        })

    }

    function animate(opts) {

        var start = new Date;
        var delta = opts.delta || linear;

        var timer = setInterval(function() {
            var progress = (new Date - start) / opts.duration;

            if (progress > 1) progress = 1;

            opts.step( delta(progress) );

            if (progress == 1) {
                clearInterval(timer);
                opts.complete && opts.complete();
            }
        }, opts.delay || 13);

        return timer;
    }

// ------------------ Delta ------------------

    function elastic(progress) {
        return Math.pow(2, 10 * (progress-1)) * Math.cos(20*Math.PI*1.5/3*progress)
    }

    function linear(progress) {
        return progress
    }

    function quad(progress) {
        return Math.pow(progress, 2)
    }

    function quint(progress) {
        return Math.pow(progress, 5)
    }

    function circ(progress) {
        return 1 - Math.sin(Math.acos(progress))
    }

    function back(progress) {
        return Math.pow(progress, 2) * ((1.5 + 1) * progress - 1.5)
    }

    function bounce(progress) {
        for(var a = 0, b = 1, result; 1; a += b, b /= 2) {
            if (progress >= (7 - 4 * a) / 11) {
                return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
            }
        }
    }

    function makeEaseInOut(delta) {
        return function(progress) {
            if (progress < .5)
                return delta(2*progress) / 2
            else
                return (2 - delta(2*(1-progress))) / 2
        }
    }

    function makeEaseOut(delta) {
        return function(progress) {
            return 1 - delta(1 - progress)
        }
    }



})();