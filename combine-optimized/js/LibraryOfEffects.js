
function AddArrow() {
    function css(elem,css){
        for (var j in css) {
            elem.style[j] = css[j];
        }
    }

    return {
        init: function (opts) {

            var _this = this;

            this.render(opts);

            var elems = document.querySelectorAll('*');

            document.addEventListener('arrowHide', function() {

                _this.onTouchstart(_this);
            });
            document.addEventListener('arrowShow', function() {
                _this.onTouchend(_this);
            });

            document.addEventListener('hideElementsEvent', function(){
                _this.onTouchstart(_this);
            });
            document.addEventListener('showElementsEvent', function() {
                _this.onTouchend(_this);
            });
        },

        render: function (opts) {
            var container = document.createElement(opts.elemName),
                img = document.createElement(opts.imageTagName);

            container.className = opts.elemClassName;
            img.style.backgroundImage = opts.arrowImgSrc;
            container.appendChild(img);
            css(container, opts.elemStyle);
            css(img, opts.imageStyle);
            opts.container ? document.querySelector(opts.container).appendChild(container) : document.body.appendChild(container);
            this.container = container;



        },

        onTouchstart: function(_this) {
            _this.container.style.display = 'none';
        },

        onTouchend: function(_this) {
            _this.container.style.display = 'block';
        }

    }
}


function AddHyperLink() {

    function setAttributes(elem, attributes) {

        for ( var prop in attributes ) {
            if ( prop != 'target') {
                elem.setAttribute( prop, attributes[prop]);
            }
        }
    }

    return {
        init: function(opts) {
            this.render(opts);
        },

        render: function(opts) {
            var div = document.createElement('div'),
                a = document.createElement('a'),
                fragment = document.createDocumentFragment();

            div.className = opts.linkWrapperClassName;

            setAttributes( a, opts.linkAttributes);

            a.innerHTML = opts.linkTitle;

            div.appendChild(a);
            fragment.appendChild(div);

            var container = document.querySelector(opts.container);

            container.appendChild(fragment);

            if (opts.linkAttributes.target) {
                a.addEventListener('touchend', function(e){
                    e.preventDefault();
                    e.stopPropagation();

                    var win = window.open(opts.linkAttributes.href, opts.linkAttributes.target);
                    win.focus();
                })
            }
        }
    }
}


function AddAudio() {
    function css(elem,css){
        for (var j in css) {
            elem.style[j] = css[j];
        }
    }

    var windowWidth = window.innerWidth;

    return {
        init: function (opts) {
            this.render(opts);
        },
        touchTarget : '',
        audioParam: false,
        globalAudio: '',
        currentAudio: '',
        render: function (opts) {
            var _this = this;

            if ( opts.globalAudio ) {
                _this.globalAudio = _this.addAudio(opts.globalAudio).querySelector('audio');
                var elem = _this.addAudio(opts.globalAudio);

                opts.container ? document.querySelector(opts.container).appendChild(elem) : document.body.appendChild(elem);


            }
            if ( opts.slidesAudio ) {
                opts.slidesAudio.forEach(function(elem) {
                    var container = document.querySelector(elem.container);
                    container.classList.add('audio-container');
                    container.appendChild(_this.addAudio(elem));
                });
            }

            var elems = document.querySelectorAll('*');

            for ( var i = 0; i < elems.length; i++ ) {
                elems[i].addEventListener('slideaudiochange', function(e){
                    _this.touchTarget = this;
                });
            }

            _this.drawSecurityScreen(opts);
            _this.drawAudioButton(opts, _this);


            document.addEventListener('hideElementsEvent', function(){
                _this.musicControls.style.display = 'none';

            });
            document.addEventListener('showElementsEvent', function() {
                _this.musicControls.style.display = 'block';
            });

        },

        addAudio: function(opts) {

            var _this = this;
            var audio = document.createElement('audio'),
                div = document.createElement('div');

            for ( var prop in opts ) {
                if ( typeof opts[prop] == 'object' || typeof opts[prop] == 'array' ) {
                    continue;
                }
                audio.setAttribute( prop, opts[prop]);
            }

            for (var i = 0; i < opts.sources.length; i++) {
                var source = document.createElement('source');
                source.setAttribute('src', opts.sources[i].src);
                source.setAttribute('type', opts.sources[i].type);
                audio.appendChild(source);
            }

            audio.autobuffer = true;
            audio.load();

            div.appendChild(audio);

            var touchEndEvent = document.createEvent('Event');
            touchEndEvent.initEvent('touchend')

            audio.addEventListener('videoEvent', function() {
                if ( _this.audioParam ) {
                    _this.tapArea.dispatchEvent(touchEndEvent);
                }

            });
            return div;
        },

        drawSecurityScreen: function(opts) {
            var _this = this;

            var screen = document.createElement('div');

            screen.classList.add('screen-music-security');

            var img = document.createElement('div'),
                p = document.createElement('div'),
                span1 = document.createElement('span'),
                span2 = document.createElement('span'),
                fragment = document.createDocumentFragment();

            img.style.background = 'url('+opts.playButtonUrl+')';
            img.style.backgroundSize = 'contain';

            span1.innerHTML = opts.silentInnerHTML;
            span2.innerHTML = opts.silenceButtonHTML;

            span1.className = opts.silentInnerClassName;
            span2.className = opts.silenceButtonClassName;

            img.style.position = 'absolute';

            img.style.width  = windowWidth/2 + 'px';
            img.style.height = windowWidth/2 + 'px';
            img.style.left = '50%';
            img.style.top = '50%';
            img.style.marginLeft = - windowWidth/4 +'px';
            img.style.marginTop = - windowWidth/4 +'px';

            p.appendChild(span1);
            p.appendChild(span2);

            screen.appendChild(img);
            screen.appendChild(p);
            fragment.appendChild(screen);
            document.body.appendChild(fragment);

            p.style.position = 'relative';
            p.style.top = img.getBoundingClientRect().bottom+'px';

            screen.addEventListener('touchmove',function(e) {
                e.preventDefault();
                e.stopPropagation();
            });

            img.addEventListener('touchend',function(e) {
                e.preventDefault();
                e.stopPropagation();

                document.body.removeChild(screen);

                _this.audioParam = true;
                _this.audioCurrentCheck(e, _this)

            });
            span2.addEventListener('touchend',function(e) {
                e.preventDefault();
                e.stopPropagation();
                document.body.removeChild(screen);
                _this.audioParam = false;
                _this.audioCurrentCheck(e, _this)

            })
        },

        drawAudioButton: function(opts, _this) {

            var div = document.createElement('div'),
                tapArea = document.createElement('div'),
                span = document.createElement('span'),
                i = document.createElement('i');

            i.className = 'icon-music';
            div.className = opts.audioIcon.className;
            i.appendChild(tapArea);
            div.appendChild(i);
            div.appendChild(span);

            if ( _this.currentAudio ) {
                div.style.display = 'block';
            } else {
                div.style.display = 'none';
            }

            _this.musicButton = i;
            _this.musicState = span;
            _this.musicControls = div;
            _this.tapArea = tapArea;

            opts.container ? document.querySelector(opts.container).appendChild(div) : document.body.appendChild(div);

            _this.notesAnimation = new _this.musicNotesAnimation(opts, _this);

            _this.tapArea.addEventListener('touchend', function(e){

                if ( _this.audioParam ) {
                    _this.audioParam = false;
                    _this.audioCurrentCheck(e, _this);
                    _this.audioShow();
                } else {
                    _this.audioParam = true;
                    _this.audioCurrentCheck(e, _this);
                    _this.audioShow();
                }

            });
        },

        musicNotesAnimation: function(opts, _this){

            var defaults = {
                steams: ['<img src="./img/musicalNotes.png"/>',
                    '<img src="./img/musicalNotes.png"/>',
                    '<img src="./img/musicalNotes.png"/>',
                    '<img src="./img/musicalNotes.png"/>',
                    '<img src="./img/musicalNotes.png"/>',
                    '<img src="./img/musicalNotes.png"/>'],

                steamsFontFamily: ["Verdana", "Geneva", "Comic Sans MS", "MS Serif", "Lucida Sans Unicode", "Times New Roman", "Trebuchet MS", "Arial", "Courier New", "Georgia"],
                steamFlyTime: 3e3,
                steamInterval: 1000,
                steamMaxSize: 30,
                steamHeight: 100,
                steamWidth: 50
            };

            function drawMusicNote(opts, _this) {
                var b = randomizer(8, m.steamMaxSize),
                    c = e(1, m.steamsFontFamily),
                    d = "#" + e(6, "0123456789ABCDEF"),
                    h = randomizer(25, 50),
                    i = randomizer(-90, 89),
                    j = g(.4, 1),
                    l = "-webkit-transform";

                l = l + ":rotate(" + i + "deg) scale(" + j + ");";

                var p = document.createElement('span'),
                    q = randomizer(0, n - m.steamWidth - b);

                p.className = 'coffee-steam z-show';
                p.innerHTML =  e(1, m.steams);

                q > h && (q = randomizer(0, h)),
                    p.style.position = "absolute";
                p.style.left = h+'px';
                p.style.top = m.steamHeight + 'px';
                p.style.fontSize = b + "px";
                p.style.color = d;
                p.style.fontFamily = c;
                p.style.display = "block";
                p.style.opacity = 1;

                p.setAttribute("style", p.getAttribute("style") + l);

                o.appendChild(p);

                var param = randomizer(m.steamHeight , m.steamHeight/2);
                animate({
                    delay: 30,
                    duration: randomizer(m.steamFlyTime / 2, 1.2 * m.steamFlyTime),
                    delta: linear(),
                    step: function(delta) {

                        p.style.top = m.steamHeight - param * ( delta )+'px';
                        p.style.left = h + q * ( 1 - delta )+'px';
                        p.style.opacity = ( 1 - delta );

                        if ( delta == 1 ) {
                            p.parentNode.removeChild(p);
                            p = null;
                        }
                    }
                });
            }

            function animate(opts) {

                var start = new Date;
                var delta = opts.delta || linear;

                var timer = setInterval(function() {
                    var progress = (new Date - start) / opts.duration;

                    if (progress > 1) progress = 1;

                    opts.step( delta(progress) );

                    if (progress == 1) {
                        clearInterval(timer);
                        opts.complete && opts.complete();
                    }
                }, opts.delay || 13);

                return timer;
            }

            function linear(progress) {
                return progress
            }
            function circ(progress) {
                return 1 - Math.sin(Math.acos(progress))
            }
            function quint(progress) {
                return Math.pow(progress, 5)
            }
            function makeEaseOut(delta) {
                return function(progress) {
                    return 1 - delta(1 - progress)
                }
            }
            var h = null,
                j = null,
                bezier = "cubic-bezier(.09,.64,.16,.94)",
                l = this,
                m = defaults,
                n = m.steamWidth,
                o = document.createElement('div');

            o.className = "coffee-steam-box";


            o.style.height = m.steamHeight+'px';

            o.style. width = m.steamWidth+'px';
            o.style.left = 0+'px';
            o.style.top = -90+'px';
            o.style.position = "absolute";
            o.style.overflow = "hidden";
            o.style.zIndex = 0;

            _this.musicControls.appendChild(o);

            function animateContainer() {
                var a = randomizer(-10, 10);

                a += parseInt(o.style.left);

                a >= 10 ? a = 10 : -10 >= a && (a = -10);

                setInterval(function(){
                    o.style.left = a * ( 1 ) +'px';
                    o.style.webkitTransition = 'left '+randomizer(1e3, 3e3)/1000+'s '+bezier+' 0s';
                    o.style.transition = 'left '+randomizer(1e3, 3e3)/1000+'s '+bezier+' 0s';

                }, (randomizer(1e3, 3e3))*2)
            }

            function e(a, b) {
                a = a || 1;
                var c = "", d = b.length - 1, e = 0;
                for (i = 0; a > i; i++)e = randomizer(0, d - 1), c += b.slice(e, e + 1);
                return c
            }

            function randomizer(a, b) {
                var c = b - a, d = a + Math.round(Math.random() * c);
                return parseInt(d)
            }

            function g(a, b) {
                var c = b - a, d = a + Math.random() * c;
                return parseFloat(d)
            }

            return {
                stop : function () {
                    clearInterval(h);
                    clearInterval(j);
                    this.audioAnimation = true;
                },
                audioAnimation: true,
                start : function () {

                    if ( !this.audioAnimation ) return;

                    h = setInterval(function () {
                        drawMusicNote()
                    }, randomizer(m.steamInterval / 2, m.steamInterval))

                    j = setInterval(function () {

                    }, randomizer(100, 1e3) + randomizer(1e3, 3e3))
                    this.audioAnimation = false;

                }
            }

        },

        audioCurrentCheck: function(e, _this) {

            var _this = this;
            if ( _this.touchTarget && _this.touchTarget.querySelector('audio') ) {
                if ( _this.audioParam ) {
                    if (_this.currentAudio) stopMedia(), _this.notesAnimation.stop();

                    _this.touchTarget.querySelector('audio').play();

                    _this.notesAnimation.start();

                    if ( _this.currentAudio ) _this.currentAudio.removeEventListener('play', _this.addPlayClass);
                    if ( _this.currentAudio ) _this.currentAudio.removeEventListener('pause', _this.removePlayClass);

                    _this.currentAudio = _this.touchTarget.querySelector('audio');

                    _this.stopMedia();
                    _this.currentAudio.addEventListener('play', _this.addPlayClass);
                    _this.currentAudio.addEventListener('pause', _this.removePlayClass);
                    _this.musicState.innerHTML = 'PLAY'

                } else {
                    if ( _this.currentAudio ) _this.currentAudio.pause(), _this.notesAnimation.stop();
                    _this.musicState.innerHTML = 'PAUSE';

                }
                _this.musicControls.style.display = 'block';

            } else {

                if ( _this.audioParam ) {
                    if ( _this.globalAudio ) {
                        if ( _this.currentAudio != _this.globalAudio ) {
                            if ( _this.currentAudio ) {
                                _this.musicControls.style.display = 'none';
                                _this.currentAudio.pause();
                                _this.notesAnimation.stop();
                            }
                        }
                        _this.musicState.innerHTML = 'PLAY';
                        if ( _this.globalAudio ) {

                            _this.stopMedia();

                            _this.globalAudio.play();
                            _this.notesAnimation.start();
                            _this.musicControls.style.display = 'block';

                        } else {
                            _this.musicControls.style.display = 'none';
                        }

                    } else {
                        _this.musicState.innerHTML = 'PAUSE';
                        _this.musicControls.style.display = 'block';

                        if ( _this.currentAudio ) {
                            _this.currentAudio.pause();
                            _this.notesAnimation.stop();
                        } else {
                            _this.musicControls.style.display = 'none';

                        }
                        if ( !_this.globalAudio ) {
                            _this.musicControls.style.display = 'none';
                        }
                    }

                    _this.currentAudio = _this.globalAudio;

                    if ( _this.currentAudio ) _this.currentAudio.addEventListener('play', _this.addPlayClass);
                    if ( _this.currentAudio ) _this.currentAudio.addEventListener('pause', _this.removePlayClass);


                } else {

                    _this.musicState.innerHTML = 'PAUSE';
                    if (_this.currentAudio) {
                        _this.currentAudio.pause();
                        _this.notesAnimation.stop();
                        _this.musicControls.style.display = 'block';
                    }
                    if ( _this.globalAudio ) {
                        _this.musicControls.style.display = 'block';
                    } else {
                        _this.musicControls.style.display = 'none';
                    }

                }
            }
        },
        stopMedia: function() {
            var _this = this;
            var mediaElements = document.querySelectorAll('audio, video');

            for (var i = 0; i < mediaElements.length; i++ ) {

                if ( _this.globalAudio != mediaElements[i] || _this.currentAudio != mediaElements[i]) {

                    mediaElements[i].pause();

                }

            }
        },
        addPlayClass: function(){

            document.querySelector('.icon-music').classList.add('play-music');
            document.querySelector('.icon-music').classList.remove('pause-music');
        },
        removePlayClass: function(){

            var _this = this;
            document.querySelector('.icon-music').classList.remove('play-music');
            document.querySelector('.icon-music').classList.add('pause-music');
        },
        audioShow: function(){
            var _this = this;
            _this.musicState.classList.add('audio-show');
            setTimeout(function(){
                _this.musicState.classList.remove('audio-show');
            }, 500)

        }
    }
}


function AddText() {
    function css(elem,css){
        for (var j in css) {
            elem.style[j] = css[j];
        }
    }

    return {
        init: function (opts, swiper) {

            var _this = this;
            this.render(opts);
            if (swiper) {
                this.addCallback(swiper);
            }

//            var els = document.getElementsByTagName('*');
//            for(var i=0 ; i<els.length ; i++){
//                els[i].addEventListener("addClassToActive", function(e) {
//                    _this.addClass(e, _this)
//                });
//            }
        },

        addClass: function (e, constructor) {
            if (constructor) {
                constructor.closeOpenContainers();
            } else {
                this.closeOpenContainers();

            }
            var elem = e.target;
            if ( elem.classList.contains('slide-active-animation') ) return;
            elem.classList.add('slide-active-animation');
        },

        render: function (opts) {
            var _this = this;

            var div = document.createElement('div'),
                div_heading = document.createElement('div'),
                div_text = document.createElement('div'),
                span = document.createElement('span'),
                touchButton = document.createElement('div'),
                h = document.createElement(opts.headingTagName),
                p = document.createElement('p');

            this.div = div;
            this.div_heading = div_heading;
            this.div_text = div_text;
            this.p = p;
            this.span = span;
            this.touchButton = touchButton;
            this.opts = opts;

            div.classList.add(opts.elemName);
            div.classList.add(opts.setVector);
            h.style.display = 'block';
            h.innerHTML = opts.heading;
            p.innerHTML = opts.text;
            p.style.overflow = 'hidden';

            span.className = 'smallTxt-arrow txt-arrow css_sprite01';

            div_heading.className = 'text-title';
            div_text.className = 'text-detail';
            touchButton.className = 'touch-area';
            h.className = opts.headingClassName;
            h.style.wordWrap = 'normal';
            h.style.whiteSpace = 'nowrap';

            div_heading.appendChild(h);
            div_heading.appendChild(span);
            span.appendChild(touchButton);

            div_text.appendChild(p);

            if (opts.heading) div.appendChild(div_heading);
            if (opts.text) div.appendChild(div_text);

            css(div, opts.style);
            css(span, opts.buttonStyle);

            var container = document.querySelector(opts.container);


            container.style.position = 'relative';
            container.appendChild(div);

            touchButton.style.webkitTransformStyle = 'preserve-3d';

            touchButton.style.webkitTransform = 'translate3d(0,0,100px)';

            this.touchButton.addEventListener('touchend', function (e) {

                _this.span.classList.contains('z-toggle') ? _this.hideAnimation(this, _this.opts) : _this.showAnimation(this, _this.opts);
            });

//            h.style.width = h.getBoundingClientRect().width+'px';

            div_heading.style.height = h.getBoundingClientRect().height+'px';




            p.style.width = opts.textWidth+'px';

            this.primaryHeight = div_heading.getBoundingClientRect().height+'px';
            this.primaryWidth = h.getBoundingClientRect().width+'px';

            if ( parseFloat(this.primaryWidth) > opts.textWidth) {
                var string = h.innerHTML;
                do {
                    string = string.substring(0, string.length-1);
                    h.innerHTML = string;

                } while ( h.getBoundingClientRect().width >= opts.textWidth );

                string = string.substring(0, string.length-3);
                string += '...';
                h.innerHTML = string;
                this.primaryWidth = opts.textWidth + 'px';
            }
//            console.log('this.primaryWidth', this.primaryWidth);
//            console.log(this.primaryHeight)
            div.style.height = this.primaryHeight;
            div.style.width = this.primaryWidth;

//            console.log( parseFloat(this.primaryWidth) > opts.textWidth);



            div.setAttribute('data-primary-width', div.style.width);
            this.elemsPosition(opts.driftCourse);

        },
        initListeners: function() {
            var _this = this;
            this.touchButton.addEventListener('touchend', function (e) {
                _this.span.classList.contains('z-toggle') ? _this.hideAnimation(this, _this.opts) : _this.showAnimation(this, _this.opts);
            });
        },
        elemsPosition: function(position){
            switch (position) {
                case 'down':
                    this.p.style.webkitTransform = 'translate3d(0px, -'+ this.p.getBoundingClientRect().height+'px,0)';
                    this.p.style.webkitTransform = 'translate3d(0px, -'+ this.p.getBoundingClientRect().height+'px,0)';
                    this.p.style.paddingTop = 0;

                    this.div_text.style.top = this.div_heading.getBoundingClientRect().height+'px';
                    break;
                case 'up':
                    this.p.style.webkitTransform = 'translate3d(0px, '+ this.p.getBoundingClientRect().height+'px,0)';
                    this.div_text.style.bottom = this.div_heading.getBoundingClientRect().height+'px';
                    this.p.style.paddingBottom = 0;
                    this.div_heading.style.bottom = 0+'px';
                    break;
            }
        },

        hideAnimation: function(elem, opts){

            elem.parentElement.classList.remove('z-toggle');
            this.div_text.classList.remove('action');
            this.div_text.classList.remove('z-show');

            switch (opts.driftCourse) {
                case 'down':
                    this.p.style.webkitTransform = 'translate3d(0px, -'+ this.p.getBoundingClientRect().height+'px,0)';
                    break;
                case 'up':
                    this.p.style.webkitTransform = 'translate3d(0px, '+ this.p.getBoundingClientRect().height+'px,0)';
                    break;
            }
            this.div.style.height = this.div_heading.getBoundingClientRect().height+'px';
            this.div.style.width = this.primaryWidth;
        },

        showAnimation: function(elem){
            elem.parentElement.classList.add('z-toggle');

            this.closeOpenContainers();

            this.div.style.height = this.div_heading.getBoundingClientRect().height + this.p.getBoundingClientRect().height+ 'px';
            this.div.style.width = this.p.getBoundingClientRect().width + 'px';
            this.p.style.webkitTransform = 'translate3d(0px, 0px,0px)';
            this.div_text.classList.add('action');
            this.div_text.classList.add('z-show');
            this.div_text.style.height = this.div_text.querySelector('p').getBoundingClientRect().height+'px';
        },

        closeOpenContainers: function() {
            var action_array = document.querySelectorAll('.action.z-show');

            if (action_array) {
                for (var i = 0; i < action_array.length; i++) {
                    action_array[i].classList.remove('action');
                    action_array[i].classList.remove('z-show');
                    action_array[i].parentElement.querySelector('.smallTxt-arrow').classList.remove('z-toggle');
                    action_array[i].style.height = '0px';
                    action_array[i].parentElement.style.height = this.primaryHeight;
                    action_array[i].parentElement.style.width = action_array[i].parentElement.getAttribute('data-primary-width');
                    console.log(action_array[i].parentElement)
                }
            }
        },

        getHeight: function(elem, opts){
            return (elem.lineHeight == 'normal') ?
                parseInt(elem.fontSize)*1.2*opts.rows+'px' :
                    elem.lineHeight / elem.lineHeight ?
                parseInt(elem.fontSize)*elem.lineHeight*opts.rows+'px' :
                /px$/.test(elem.lineHeight) ?
                    parseInt(elem.lineHeight)*opts.rows+'px' :
                    parseInt(elem.fontSize)*parseInt(elem.lineHeight)/100*opts.rows +'px'
        }

    }

}

function AddVideo() {
    function css(elem,css){
        for (var j in css) {
            elem.style[j] = css[j];
        }
    }

    return {
        init: function (opts) {

            var _this = this;
            this.render(opts);

            document.addEventListener('videoStop', function() {
                _this.videoStop();
            });
        },

        addClass: function (e, constructor) {
            if (constructor) {
                constructor.closeOpenContainers();
            } else {
                this.closeOpenContainers();
            }
            var elem = e.target;
            if ( elem.classList.contains('slide-active-animation') ) return;
            elem.classList.add('slide-active-animation');
        },

        render: function (opts) {
            var _this = this;

            var container = document.querySelector(opts.container);

            var video = document.createElement('video'),
                div = document.createElement('div');

            this.video = video;
            div.className = opts.elemName;

            for ( var prop in opts.videoAttr ) {
                video.setAttribute( prop, opts.videoAttr[prop]);
            }

            for (var i = 0; i < opts.sources.length; i++) {
                var source = document.createElement('source');
                source.setAttribute('src', opts.sources[i].src);
                source.setAttribute('type', opts.sources[i].type);
                video.appendChild(source);
            }

            css(div, opts.style);

            var videoEvent = document.createEvent('Event');
            videoEvent.initEvent('videoEvent');

            video.addEventListener('touchend', function(){
                video.play();
            });

            video.addEventListener('play', function(e){

                var mediaElements = document.querySelectorAll('audio, video');

                for (var i = 0; i < mediaElements.length; i++ ) {
                    if ( video == mediaElements[i] ) continue;

                    mediaElements[i].dispatchEvent(videoEvent);
                }
                video.play();

//                e.preventDefault();
//                e.stopPropagation();
            });

            div.appendChild(video);
            container.appendChild(div);


            div.style.position = 'absolute';
//            div.style.textAlign = 'center';
            video.style.position = 'relative';
            video.style.zIndex = 1000;
//            video.style.left = '50%';
//            video.style.marginLeft = '-'+video.width/2+'px';
            div.style.webkitTransform = 'translate3d(0px, 0px, 0px)';
//            div.style.webkitTransformStyle = 'preserve-3d';

        },

        videoStop: function (e) {
            this.video.pause();
        },

        getHeight: function(elem, opts){
            return (elem.lineHeight == 'normal') ?
                parseInt(elem.fontSize)*1.2*opts.rows+'px' :
                    elem.lineHeight / elem.lineHeight ?
                parseInt(elem.fontSize)*elem.lineHeight*opts.rows+'px' :
                /px$/.test(elem.lineHeight) ?
                    parseInt(elem.lineHeight)*opts.rows+'px' :
                    parseInt(elem.fontSize)*parseInt(elem.lineHeight)/100*opts.rows +'px'
        }
    }
}

var Enlarge = function() {

    var startX, startY, xCoord, yCoord, xCoordStart, yCoordStart, xCoordEnd, yCoordEnd;

    return {
        opts: {},
        touchImgParam: false,               // detect image enlarged or not
        swipeImgParam: false,               // detect swiping under image
        enlargeParametr: false,             // block events during enlarge animation
        init: function(opts) {
            var _this = this;

            this.opts = opts;

            _this.elem = document.querySelector(opts.elem);

            _this.elem.addEventListener('touchstart', function(e){
                e.touches ||  e.originalEvent ? e = e.touches[0] : null;

                if (_this.touchImgParam) {
                    startX = e.pageX;
                    startY = e.pageY;
                } else {
                    _this.opts.pointX = (e.pageX) / window.innerWidth;
                    _this.opts.pointY = (e.pageY) / window.innerHeight;

                }
            });
            var mylatesttap;

            _this.elem.addEventListener('touchend', function(e){

                if ( _this.enlargeParametr ) return;

                if ( _this.elem.classList.contains('enlarged') ) {
                    if ( _this.swipeImgParam ) {
                        _this.swipeImgParam = false;
                        return;
                    }
                    _this.enlargeCancel();
                } else {

                    var now = new Date().getTime();
                    var timesince = now - mylatesttap;

                    if((timesince < 600) && (timesince > 0)){
                        _this.enlarge()

                    }
                    mylatesttap = new Date().getTime();

                }
            });
            _this.elem.addEventListener('touchmove', function(e){
                if (!_this.touchImgParam || _this.enlargeParametr ) return;

                e.preventDefault();
                e.stopPropagation();
                _this.swipeImgParam = true;

                e.touches ||  e.originalEvent ? e = e.touches[0] : null;

                var currentX = e.pageX,
                    currentY = e.pageY;

                _this.elem.style.webkitTransition = 'all 0s linear';
                _this.elem.style.transition = 'all 0s linear';

                if ( !startX ) startX = currentX;
                xCoord +=  (currentX-startX) / _this.opts.imgScale;

                if ( !startY ) startY = currentY;

                yCoord +=  (currentY-startY) / _this.opts.imgScale;

                if ( xCoord < xCoordStart) xCoord = xCoordStart;
                if ( xCoord > xCoordEnd) xCoord = xCoordEnd;
                if ( yCoord < yCoordStart) yCoord = yCoordStart;
                if ( yCoord > yCoordEnd) yCoord = yCoordEnd;

                _this.elem.style.webkitTransform = 'scale('+_this.opts.imgScale
                    + ') translate3d(' + xCoord + 'px, ' + yCoord + 'px,0)';

                _this.elem.style.webkitTransition = 'all 0s linear';
                _this.elem.style.transition = 'all 0s linear';

                startX = currentX;
                startY = currentY;
            });

        },
        enlarge: function(){

            var _this = this;
            _this.touchImgParam = true;

            var params = _this.elem.getBoundingClientRect();

            if ( _this.elem.classList.contains('enlarged')
                || !document.querySelector(_this.opts.container).classList.contains('sw-slide-active')) return;

            _this.elem.classList.add('enlarged');

            xCoord = (0.5-_this.opts.pointX) * (params.width*_this.opts.imgScale - window.innerWidth);
            xCoordStart = (-0.5) * (params.width*_this.opts.imgScale - window.innerWidth) / _this.opts.imgScale;
            xCoordEnd = (0.5) * (params.width *_this.opts.imgScale - window.innerWidth) / _this.opts.imgScale;

            yCoord = (0.5-_this.opts.pointY) * ( params.height*_this.opts.imgScale - window.innerHeight);
            yCoordStart = (-0.5) * ( params.height*_this.opts.imgScale - window.innerHeight) / _this.opts.imgScale;
            yCoordEnd = (0.5) * ( params.height*_this.opts.imgScale - window.innerHeight) / _this.opts.imgScale;

            if ( xCoord < xCoordStart) xCoord = xCoordStart;
            if ( xCoord > xCoordEnd) xCoord = xCoordEnd;
            if ( yCoord < yCoordStart) yCoord = yCoordStart;
            if ( yCoord > yCoordEnd) yCoord = yCoordEnd;

            _this.elem.style.webkitTransform = 'scale('
                + this.opts.imgScale
                + ') translate3d(' + xCoord + 'px, ' + yCoord + 'px,0)';

            _this.elem.style.webkitTransition = 'all '+this.opts.duration+'s '+this.opts.effect;

            _this.blocker();

        },
        enlargeCancel: function() {
            if (!this.elem) return;

            this.touchImgParam = false;
            this.elem.classList.remove('enlarged');
            this.elem.style.webkitTransition = 'all '+this.opts.duration+'s linear';
            this.elem.style.transition = 'all '+this.opts.duration+'s linear';
            this.elem.style.webkitTransform = 'scale(1) translate3d(0,0,0)';
            this.blocker();
        },
        blocker: function() {
            var _this = this;
            _this.enlargeParametr = true;
            setTimeout(function(){
                _this.enlargeParametr = false;
            }, this.opts.duration * 1000);
        }
    }
}

function EnlargeByCreator() {
    return {
        init: function(opts) {
            var _this = this;
            _this.opts = opts;
            _this.elem = document.querySelector(opts.elem);
        },
        enlarge: function(){
            var _this = this;
            var params = _this.elem.getBoundingClientRect();
            if ( _this.elem.classList.contains('enlarged') ) return;
            _this.elem.classList.add('enlarged');
            _this.elem.style.webkitTransform = 'scale('+_this.opts.imgScale
                + ') translate('
                + (0.5-_this.opts.pointX)
                * (params.width*_this.opts.imgScale - window.innerWidth)
                / _this.opts.imgScale
                + 'px, '
                + (0.5-_this.opts.pointY)
                * ( params.height*_this.opts.imgScale - window.innerHeight)
                / _this.opts.imgScale
                + 'px)';
            _this.elem.style.webkitTransition = 'all '+_this.opts.duration+'s '+_this.opts.effect;
        },
        enlargeCancel: function() {
            if (!this.elem) return;
            this.elem.classList.remove('enlarged');
            this.elem.style.webkitTransition = 'all 0s ease';
            this.elem.style.webkitTransform = 'scale(1) translate(0,0)';
        }
    }
}

function ObjectRolling(){
    return {
        init: function(opts) {
            this.render(opts);
        },
        render: function(opts){
            var wrapper = document.createElement('div'),
                div = document.createElement('div'),
                img = document.createElement('img'),
                imgPopup = document.createElement('img'),
                fragment = document.createDocumentFragment(),
                imageWrapper = document.createElement('div'),
                a = document.createElement('a');

            wrapper.className = 'm-link';
            imageWrapper.className = 'u-maskLayer m-weixinShareLayer hide';
            a.className = 'u-maskLayer-close';

            div.className = 'imgLink';
            img.setAttribute('src', opts.rollingImgSrc);
            imgPopup.setAttribute('src', opts.PopupImgSrc);

            imageWrapper.style.display = 'none';

            div.appendChild(img);
            imageWrapper.appendChild(imgPopup);
            imageWrapper.appendChild(a);

            wrapper.appendChild(div);
            fragment.appendChild(wrapper);

            fragment.appendChild(imageWrapper);

            var container = document.querySelector(opts.container);

            container.appendChild(fragment);

            img.addEventListener('touchend', function(){

                if ( imageWrapper.classList.contains('show') ) {
                    hide()
                } else {
                    imageWrapper.style.display = 'block';
                    setTimeout(function(){
                        imageWrapper.classList.add('show');
                    }, 1)
                }

            })

            imageWrapper.addEventListener('touchend', hide );
            a.addEventListener('touchend', hide );

            function hide() {
                imageWrapper.classList.remove('show');
                setTimeout(function(){
                    imageWrapper.style.display = 'none';
                }, 600);
            }

        }
    }
}

function PageCounter(){
    return {
        opts: {},
        slides: [],
        init: function(opts) {
            this.render(opts);
        },
        render: function(opts){
            var progressWrapper = document.createElement('div'),
                div = document.createElement('div'),
                pageQuantitySpan = document.createElement('span'),
                pageConterSpan = document.createElement('span'),
                slides = document.querySelectorAll(opts.slide);

            this.opts = opts;
            div.className = 'page-number-counter';
            progressWrapper.appendChild(div);

            opts.container ?
                document.querySelector(opts.container).insertBefore(progressWrapper, document.querySelector(opts.container).firstChild) :
                document.body.appendChild(progressWrapper);

            this.pageQuantitySpan = pageQuantitySpan;
            this.pageConterSpan = pageConterSpan;
            this.progressLine = div;

            div.appendChild(pageConterSpan);
            div.appendChild(pageQuantitySpan);



            for ( var i = 0; i < ( slides.length ); i++ ) {
                this.slides.push(slides[i]);
            }


            this.pageNumber(opts);
        },
        pageNumber: function(opts){

            var currnetSlide = document.querySelector(this.opts.activeSlide),
                current = 0;

            for ( var i = 0; i < ( this.slides.length ); i++ ) {

                if ( this.slides[i] === currnetSlide ) {

                    current = i;

                    if ( i == (this.slides.length - 1) ) {
                        current = 1;
                    }
                    if ( i == 0 ) {
                        current = this.slides.length - 2;
                    }
                    this.pageConterSpan.innerHTML = current + ' / ';
                    this.pageQuantitySpan.innerHTML = this.slides.length - 2;
                }
            }
        }
    }
}

function PhotoAlbum (opts){
    function animate(opts) {

        var start = new Date;
        var delta = opts.delta || linear;

        var timer = setInterval(function() {
            var progress = (new Date - start) / opts.duration;

            if (progress > 1) progress = 1;

            opts.step( delta(progress) );

            if (progress == 1) {
                clearInterval(timer);
                opts.complete && opts.complete();
            }
        }, opts.delay || 13);

        return timer;
    }

    function linear(progress) {
        return progress
    }
    function circ(progress) {
        return 1 - Math.sin(Math.acos(progress))
    }
    function quint(progress) {
        return Math.pow(progress, 5)
    }
    function makeEaseOut(delta) {
        return function(progress) {
            return 1 - delta(1 - progress)
        }
    }
    return {
        init: function(opts){

            this.container = document.querySelector(opts.container);

            this.render(opts);

        },
        render: function(opts){

            var _this = this;

            var fragment = document.createDocumentFragment(),
                div = document.createElement('div'),
                ul = document.createElement('ul');

            _this.options = opts;
            _this.imgContainer = ul;

            div.className = 'page-content';
            ul.className = 'm-cascadingTeletext';
            ul.style.height = window.innerHeight + 'px';

            opts.images.forEach(function(url){
                _this.createImgContainetComponent(url);
            });

            this.renderControlBtns(div);

            div.appendChild(ul);
            fragment.appendChild(div);

            this.container.appendChild(fragment);

            this.container.classList.add('mag-current');

            _this.swipeParam = 0;
            _this.swipeStart = { x: 0, y: 0 };
            _this.swipeEnd = { x: 0, y: 0 };

            _this.imgContainer.addEventListener('touchstart', function(e){
//                e.preventDefault();
//                e.stopPropagation();

                e.touches ||  e.originalEvent ? e = e.touches[0] : null;
                _this.swipeStart.x = e.pageX || e.clientX;
                _this.swipeStart.y = e.pageY || e.clientY;

            });

            _this.imgContainer.addEventListener('touchmove', function(e){

                var event = e;

                _this.swipeParam++;
                e.touches ||  e.originalEvent ? e = e.touches[0] : null;
                _this.swipeEnd.x = e.pageX || e.clientX;
                _this.swipeEnd.y = e.pageY || e.clientY;

                console.log(_this.swipeParam, _this.swipeStart.y, _this.swipeEnd.y)
                if ( _this.swipeParam >= Math.abs(_this.swipeStart.y -_this.swipeEnd.y)/16 ) {
                    event.preventDefault();
                    event.stopPropagation();

                    console.log('up!!!!')
                } else {

                }

            });
            _this.imgContainer.addEventListener('touchend', function(e){

                e.touches ||  e.originalEvent ? e = e.touches[0] : null;


                if ( _this.swipeEnd.x == 0 ) _this.swipeEnd.x = _this.swipeStart.x;

                _this.swipeParam = Math.abs(_this.swipeStart.x - _this.swipeEnd.x);

                if ( _this.swipeParam > window.innerWidth/10 && _this.swipeEnd.x < _this.swipeStart.x) {
                    _this.swipeNext(_this);

                }
                if ( _this.swipeParam > window.innerWidth/10 && _this.swipeEnd.x > _this.swipeStart.x) {
                    _this.swipePrev(_this);
                }
                _this.swipeParam = 0;
                _this.swipeEnd.x = 0
            });
        },
        renderControlBtns: function(elem){
            var _this = this,
                prevDiv = document.createElement('div'),
                nextDiv = document.createElement('div'),
                prevDivTouchArea = document.createElement('div'),
                nextDivTouchArea = document.createElement('div');

            prevDiv.className = 'u-guidePrev z-move';
            nextDiv.className = 'u-guideNext z-move';

            prevDiv.appendChild(prevDivTouchArea);
            nextDiv.appendChild(nextDivTouchArea);

            elem.appendChild(prevDiv);
            elem.appendChild(nextDiv);

            prevDivTouchArea.addEventListener('touchend', function(e){
                _this.swipePrev(_this);
            });
            nextDivTouchArea.addEventListener('touchend', function(e){
                _this.swipeNext(_this);
            });
        },
        swipePrev: function(_this){
            _this.imgContainer.children[_this.imgContainer.children.length - 1].swipePrev();
        },
        swipeNext: function(_this){
            _this.imgContainer.children[0].swipeNext();
        },
        createImgContainetComponent: function(url){

            var _this = this,
                li = document.createElement('li'),
                img = document.createElement('img');

            img.setAttribute('src', url);

            li.swipeNext = function(){
                var _li = this;
                _li.classList.add('z-hideToLeft');

                setTimeout(function(){
                    setTimeout(function(){
                        _li.classList.remove('z-hideToLeft');
                    },0)
                    setTimeout(function(){
                        _this.imgContainer.appendChild(_this.imgContainer.children[0]);
                    },1)
                }, 300);
            };
            li.swipePrev = function(){
                var _li = this;
                _this.imgContainer.insertBefore(_this.imgContainer.children[_this.imgContainer.children.length - 1], _this.imgContainer.children[0]);

                _li.classList.add('z-hideToRight');
                setTimeout(function(){
                    _li.classList.remove('z-hideToRight');
                }, 900);
            };

            li.appendChild(img);
            this.imgContainer.appendChild(li);
        }
    }
}

function Progressline(){
    return {
        opts: {},
        slides: [],
        init: function(opts) {
            this.render(opts);
        },
        render: function(opts){
            var progressWrapper = document.createElement('div'),
                div = document.createElement('div'),
                slides = document.querySelectorAll(opts.slide);

            this.opts = opts;
            div.className = 'progress-line';
            progressWrapper.appendChild(div);
            document.body.appendChild(progressWrapper);
            this.progressLine = div;

            for ( var i = 0; i < ( slides.length ); i++ ) {
                this.slides.push(slides[i]);
            }


            this.progressWidth(opts);
        },
        progressWidth: function(opts){

            var currnetSlide = document.querySelector(this.opts.activeSlide),
                current = 0;

            for ( var i = 0; i < ( this.slides.length ); i++ ) {

                if ( this.slides[i] === currnetSlide ) {

                    current = i;

                    if ( i == (this.slides.length - 1) ) {
                        current = 1;
                    }
                    if ( i == 0 ) {
                        current = this.slides.length;
                    }

                    this.progressLine.style.width = current / ( this.slides.length - 2 ) * 100 +'%';

                }
            }
        }
    }
}

function Smudge(opts) {
    var img = new Image();
    img.src = opts.smudge_material_src;

    var canvas = document.createElement('canvas'),
        container;
    var context = canvas.getContext('2d');


    var clickX = new Array(),
        clickY = new Array(),
        clickDrag = new Array(),
        paint,
        clickSize = new Array(),
        curSize = "normal";

    var hideElementsEvent =  document.createEvent('Event');
    hideElementsEvent.initEvent('hideElementsEvent');

    var showElementsEvent =  document.createEvent('Event');
    showElementsEvent.initEvent('showElementsEvent');

    if (opts.container){
        container = document.querySelector(opts.container)
    }  else {
        container = document.body;
    }

    container.appendChild(canvas);
    canvas.setAttribute('width', window.innerWidth);
    canvas.setAttribute('height', window.innerHeight);
    canvas.style.position = 'absolute';
    canvas.style.width = window.innerWidth+'px';
    canvas.style.height = window.innerHeight+'px';
    canvas.style.left = '0px';
    canvas.style.top = '0px';
    canvas.style.zIndex = 100000;
    canvas.style.marginRight = '1px';

    img.addEventListener('load', function () {
        context.drawImage(img, 0, 0, canvas.width, canvas.height);
    });

    var cancelCombaining = false,
        param = 0,
        trigger = true,
        paramTrigger = true;

    function canvasTouchstart(e) {
        e.preventDefault();
        e.stopPropagation();

        e.touches || e.originalEvent ? e = e.touches[0] : null;

        canvas.style.position = 'fixed';

        var mouseX = e.pageX - this.offsetLeft;
        var mouseY = e.pageY - this.offsetTop;

        paint = true;

        addClick(e.pageX, e.pageY);
        redraw();

        cancelCombaining = true;
    }

    function canvasTouchmove(e) {
        e.preventDefault();
        e.stopPropagation();

        canvas.style.position = 'fixed';

        param++;
        if (param) {
            e.touches || e.originalEvent ? e = e.touches[0] : null;

            if (paint) {

                addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
                redraw();
            }

        }

    }

    function canvasTouchend(e) {

        e.preventDefault();
        e.stopPropagation();

        if (!paramTrigger) return;

        var alpha_0 = [];
        param = 0;
        e.touches || e.originalEvent ? e = e.touches[0] : null;

        var data = context.getImageData(0, 0, canvas.width, canvas.height).data;

        for (var i = 0, n = data.length; i < n; i += 4) {
            if (data[i + 3] == 0) {
                alpha_0.push(data[i + 3])
            }
        }

        if (data.length / 4 * opts.field_area < alpha_0.length) {
            makeTransperent(); // start from hear
        }
        paint = false;
        cancelCombaining = false;
    }

    canvas.addEventListener('touchstart', canvasTouchstart);
    canvas.addEventListener('touchmove', canvasTouchmove);
    canvas.addEventListener('touchend', canvasTouchend);

    if (opts.container) {
        document.addEventListener('canvasEvent', canvasEvent)
    }

    function canvasEvent() {
        if (canvas == '') return;
        if (canvas.getBoundingClientRect().top == 0 && trigger) {
            document.dispatchEvent(hideElementsEvent);
        }
    }

    function addClick(x, y, dragging) {

        clickX.push(x);
        clickY.push(y);
        clickDrag.push(dragging);
        clickSize.push(curSize);
    }

    function redraw() {

        for (var i = 0; i < 7; i++) {

            context.beginPath();

            context.globalCompositeOperation = 'destination-out';
            strokeStyle = "rgba(0,0,0,0)";
            context.lineJoin = "round";
            context.lineWidth = opts.lineWidth;

            context.arc(clickX[clickX.length - 1], clickY[clickY.length - 1], context.lineWidth, 0, 2 * Math.PI, true);

            context.fill();
            context.closePath();

            context.beginPath();

            context.lineWidth = context.lineWidth * 2;

            if (clickDrag[i] && i && cancelCombaining) {
                context.moveTo(clickX[clickX.length - 2], clickY[clickY.length - 2]);
            } else {
                context.moveTo(clickX[clickX.length - 1], clickY[clickY.length - 1]);
            }

            context.lineTo(clickX[clickX.length - 1], clickY[clickY.length - 1]);
            context.stroke();
            context.lineWidth = context.lineWidth / 2;
            context.globalCompositeOperation = 'destination-over';
            context.closePath();
        }
        context.save();

    }

    function makeTransperent() {
        paramTrigger = false;
        animate({
            delay: 10,
            duration: opts.duration,
            delta: quad(bounce),
            step: function (delta) {
                canvas.style.opacity = 1 - delta;
                if (delta == 1) {

                    if (opts.container) {
                        document.dispatchEvent(showElementsEvent);
                    }
                    canvas.style.display = 'none';
                    trigger = false;
                    clickX = [];
                    clickY = [];
                    clickDrag = [];
                    clickSize = [];
                }
            }
        })
    }


    function animate(opts) {

        var start = new Date;
        var delta = opts.delta || linear;

        var timer = setInterval(function () {
            var progress = (new Date - start) / opts.duration;

            if (progress > 1) progress = 1;

            opts.step(delta(progress));

            if (progress == 1) {
                clearInterval(timer);
                opts.complete && opts.complete();
            }
        }, opts.delay || 13);

        return timer;
    }

// ------------------ Delta ------------------

    function elastic(progress) {
        return Math.pow(2, 10 * (progress-1)) * Math.cos(20*Math.PI*1.5/3*progress)
    }

    function linear(progress) {
        return progress
    }

    function quad(progress) {
        return Math.pow(progress, 2)
    }

    function quint(progress) {
        return Math.pow(progress, 5)
    }

    function circ(progress) {
        return 1 - Math.sin(Math.acos(progress))
    }

    function back(progress) {
        return Math.pow(progress, 2) * ((1.5 + 1) * progress - 1.5)
    }

    function bounce(progress) {
        for(var a = 0, b = 1, result; 1; a += b, b /= 2) {
            if (progress >= (7 - 4 * a) / 11) {
                return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
            }
        }
    }

    function makeEaseInOut(delta) {
        return function(progress) {
            if (progress < .5)
                return delta(2*progress) / 2
            else
                return (2 - delta(2*(1-progress))) / 2
        }
    }

    function makeEaseOut(delta) {
        return function(progress) {
            return 1 - delta(1 - progress)
        }
    }
}


function TurnAroundEffect() {

    return {
        init: function(opts) {
            this.render(opts);
        },
        param:  0,
        mouse: {
            start: 0,
            end: 0
        },
        render: function(opts) {
            var _this = this,
                url_data = opts.url_data,
                container = document.querySelector(opts.container),
                img_container = document.createElement('div');

            img_container.className = 'imgbox';

            url_data.forEach(function(url,i) {
                var img = new Image();
                img.src = url;
                if ( i == 0 ) {
                    img.style.display = 'inline';
                } else {
                    img.style.display = 'none';
                }

                img_container.appendChild(img);
            });

            container.appendChild(img_container);

            container.addEventListener('touchstart', function(evt) {
                evt.preventDefault();

                delete _this.mouse.last;

                evt.touches ||  evt.originalEvent ? evt = evt.touches[0] : null;

                _this.mouse.start = evt.pageX;

                container.addEventListener('touchmove', function(e){
                    _this.onMouseMove(e, _this);
                });
            });

            container.addEventListener('touchend', function () {
                container.removeEventListener('touchmove', function(e){
                    _this.onMouseMove(e, _this);
                });
            });
        },
        onMouseMove: function (event, _this) {

            event.preventDefault();
            event.touches ||  event.originalEvent ? event = event.touches[0] : null;

            var array = document.querySelectorAll('.imgbox img');
            if ( _this.param >= 6 ) {
                _this.param = 0;

                for ( var i = 0; i < array.length; i++ ) {

                    if (  array[i].style.display == 'inline' ) {
                        array[i].style.display = 'none';
                        if (array[i + 1]) {
                            array[i + 1].style.display = 'inline';
                            break;
                        } else {
                            array[0].style.display = 'inline';
                            break;
                        }
                    }
                }
            } else if ( _this.param <= -6 ) {
                _this.param = 0;

                for ( var i = 0; i < array.length; i++ ) {

                    if (  array[i].style.display == 'inline' ) {
                        array[i].style.display = 'none';
                        if (array[i - 1]) {
                            array[i - 1].style.display = 'inline';
                            break;
                        } else {
                            array[array.length - 1].style.display = 'inline';
                            break;
                        }
                    }
                }
            } else {
                _this.param += _this.mouse.start - event.pageX;
            }
            _this.mouse.start = event.pageX;
        }
    }
}

