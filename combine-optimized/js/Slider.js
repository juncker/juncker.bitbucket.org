
function Slider (opts){
    var container = document.querySelector(opts.container),
        html = document.querySelector(opts.container).innerHTML;

    var wrapper = '<section class="swiper-container">\
                <div class="wrapper">\
                <section class="nav-slide sw-left-sidebar ">\
                    <div class="slide-list">\
                        <div class="slide-wrapper"></div>\
                    </div>\
                </section>\
                <section class="nav-slide sw-container p-index " style="">\
                    <div id="stipes_button"></div>\
                    <div id="drops_button"></div>\
                    <div class="sw-container-pagination"></div>\
                    <div class="swiper-container-pagination"></div>\
                    <div class="sw-wrapper">'+html+'\
                </section>\
                <section class="nav-slide menu-list sw-right-sidebar"></section>\
            </div>\
        </section>';

    container.innerHTML = '';
    container.innerHTML = wrapper;

    var
        div_menu = document.createElement('div'),
        ul_menu = document.createElement('ul');

    div_menu.className = 'menu-container';
    ul_menu.className = 'menu-wrapper';

    var sw_container = document.querySelector('.sw-container');

    var defaults = {
        slideClass: 'swipe-slide-item',
        menuitems: [
            {
                tagName: 'div',
                innerHTML: 'Lorem ipsum',
                attributes: {
                    href: '#'
                }
            }
        ]
    };

    opts = opts || {};

    for (var prop in defaults) {
        if (prop in opts && typeof opts[prop] === 'object') {
            for (var subProp in defaults[prop]) {
                if (! (subProp in opts[prop])) {
                    opts[prop][subProp] = defaults[prop][subProp];
                }
            }
        }
        else if (! (prop in opts)) {
            opts[prop] = defaults[prop];
        }
    }

    opts.menuitems.forEach(function(item){

        var li = document.createElement('li'),
            elem = document.createElement( item.tagName || defaults.menuitems[0].tagName);
        li.className = 'menu-slide';
        elem.innerHTML = item.innerHTML;
        for ( var attr in item.attributes) {
            elem.setAttribute( attr , item.attributes[attr]);
        }

        li.appendChild(elem);
        ul_menu.appendChild(li);
    });
    div_menu.appendChild(ul_menu);

    document.querySelector('.menu-list').appendChild(div_menu);


    container.style.width = window.innerWidth+"px";

    var
        leftSidebar     = container.querySelector('.sw-left-sidebar'),
        rightSidebar    = container.querySelector('.sw-right-sidebar'),
        slidesContainer = container.querySelector('.sw-container');

    leftSidebar.style.width = Math.round(window.innerWidth*0.7)+"px";
    leftSidebar.style.height = window.innerHeight+"px";

    /*
     leftSidebar.addEventListener('touchend', function(e){
     e.preventDefault();
     e.stopPropagation();
     });
     */

    slidesContainer.style.width = window.innerWidth+"px";
    slidesContainer.style.height = window.innerHeight+"px";

    rightSidebar.style.width = window.innerWidth*0.7+"px";
    rightSidebar.style.height = window.innerHeight+"px";

    container.querySelector('.nav-slide .menu-container').style.height = window.innerHeight-(parseFloat(getComputedStyle(document.querySelector('.menu-container')).padding)*2)+"px";

    var children = container.querySelector('.sw-wrapper').children;

    var div = document.createElement('div');

    var eventYoch = document.createEvent('MouseEvents');
    eventYoch.initMouseEvent('click');


    var touchstart = document.createEvent('Events');
    touchstart.initEvent('touchstart');

    var touchmove = document.createEvent('Events');
    touchmove.initEvent('touchmove');

    for (var i = 0; i < children.length; i++) {
        var elem = children[i].cloneNode(true);

        if (elem.classList.contains(opts.slideClass)) {
            elem.style.width = window.innerWidth*0.3+"px";
            elem.style.height = window.innerWidth*0.3*window.innerHeight/window.innerWidth+"px";
            elem.style.margin = '15px';
            elem.className = 'show-elem';


            div.style.height = window.innerWidth*0.3*window.innerHeight/window.innerWidth+27+"px";
            div.className = 'slide-slide';


            if ( div.children.length >= 2 ) {
                container.querySelector('.slide-wrapper').appendChild(div);

                div = document.createElement('div');
                div.style.height = window.innerWidth*0.3*window.innerHeight/window.innerWidth+27+"px";

                div.className = 'slide-slide';
                div.appendChild(elem);
            } else {

                div.appendChild(elem);


            }
            if ( i+1 == children.length ) {
                div.className = 'slide-slide';
                container.querySelector('.slide-wrapper').appendChild(div);
            }

            (function(i){
                var a;
                elem.addEventListener('touchstart', function(e) {
                    a = e.target;
                });

                elem.addEventListener('touchmove', function(e) {
                    a = false;
                });
                elem.addEventListener('touchend', function(e) {
//                    e.preventDefault();
//                    e.stopPropagation();

                    if ( a != e.target ) return;

                    container.querySelector('.sw-container-pagination').children[i].dispatchEvent(eventYoch);
                    a = false;
                    pagecounter.pageNumber();
                    progressline.progressWidth();

                });
            })(i);
        }
    }

    var horizontalSwiperParent = new Swiper('.swiper-container',{
        mode: 'horizontal',
        speed: 300,
        slideElement: 'div',
        slideClass: 'nav-slide',
        slideActiveClass: 'swiper-slide-active',
        slideVisibleClass: 'swiper-slide-visible',
        slideDuplicateClass: 'swiper-slide-duplicate',
        wrapperClass: 'wrapper',
        slidesPerView: 'auto',
        initialSlide: 1,
        pagination: '.swiper-container-pagination',
        paginationClickable: true,
        touchRatio: 0,
        paginationspeed: 800

    });

    var verticalSwiperNestedSwContainer = new Swiper('.sw-container',{
        mode: 'vertical',
        speed: 200,
        slideClass: opts.slideClass,
        slideActiveClass: 'sw-slide-active',
        slideVisibleClass: 'sw-slide-visible',
        slideDuplicateClass: 'sw-slide-duplicate',
        wrapperClass: 'sw-wrapper',
        mousewheelControl : true,
        pagination: '.sw-container-pagination',
        paginationClickable: true,
        paginationspeed: 0,
        longSwipesRatio: 0.2,
        loop: true,
        progress:true,

        onProgressChange: function(swiper){
            for (var i = 0; i < swiper.slides.length; i++){
                var slide = swiper.slides[i];
                var progress = slide.progress;
                if ( slide.classList.contains('sw-slide-active') ) {
                    var videos = slide.querySelectorAll('video');
                    if ( videos.length > 0 ) {
                        // some features in future
                    } else {
                        swiper.setTransform(slide,'scale('+(1 - Math.abs(progress))+') ' );
                    }
//                    slide.style.webkitTransition = 'none';
                    slide.style.webkitTransformOrigin = '50%' +((progress > 0) ?  ' 100% ' : ' 0 ')+'0' ;
                } else {
                    swiper.setTransform(slide,'translate3d(0,0,0)');
                    swiper.setTransform(slide,'scale(1)');
                }
            }
        },
        onTouchStart:function(swiper){
            for (var i = 0; i < swiper.slides.length; i++){
                swiper.setTransition(swiper.slides[i], opts.durartion);
            }
        }

    });

    var addClassToActive = document.createEvent('Event');
    addClassToActive.initEvent('addClassToActive');

    verticalSwiperNestedSwContainer.addCallback('SwiperCreated', function(swiper){
        swiper.visibleSlides[0].dispatchEvent(addClassToActive);
    });

    var videoStop =  document.createEvent('Event');
    videoStop.initEvent('videoStop');

    var arrowShow =  document.createEvent('Event');
    arrowShow.initEvent('arrowShow', false, false);

    var arrowHide =  document.createEvent('Event');
    arrowHide.initEvent('arrowHide', false, false);

    var closeOpenContainers =  document.createEvent('Event');
    closeOpenContainers.initEvent('closeOpenContainers', false, false);

    var canvasEvent =  document.createEvent('Event');
    canvasEvent.initEvent('canvasEvent', false, false);


    verticalSwiperNestedSwContainer.addCallback('Init', function(swiper){
        swiper.visibleSlides[0].dispatchEvent(addClassToActive);
    });

    verticalSwiperNestedSwContainer.addCallback('SlideChangeEnd', function(swiper){
        var videos = document.querySelectorAll('video');

        document.dispatchEvent(canvasEvent);
        progressline.progressWidth();
        pagecounter.pageNumber();
        for (var i = 0; i < videos.length; i++) {
            videos[i].pause();
        }

    });
    verticalSwiperNestedSwContainer.addCallback('SlideChangeStart', function(swiper){

        swiper.visibleSlides[0].dispatchEvent(addClassToActive);
        document.dispatchEvent(closeOpenContainers);
        enlarge_by_creator.enlargeCancel(enlarge_by_creator);

        if ( swiper.visibleSlides[0].classList.contains('slide-active-animation') ) return;
        swiper.visibleSlides[0].classList.add('slide-active-animation');

        enlarge.enlargeCancel(enlarge);

    });

    verticalSwiperNestedSwContainer.addCallback('TouchMove', function(swiper){
        document.dispatchEvent(arrowHide);

    });

    verticalSwiperNestedSwContainer.addCallback('TouchEnd', function(swiper){
        document.dispatchEvent(arrowShow);
        enlarge_by_creator.enlarge(enlarge_by_creator);

    });


    var verticalSwiperNestedMenuContainer = new Swiper('.menu-container',{
        mode: 'vertical',
        speed: 200,
        slideElement: 'li',
        slideClass: 'menu-slide',
        slideActiveClass: 'menu-slide-active',
        slideVisibleClass: 'menu-slide-visible',
        slideDuplicateClass: 'menu-slide-duplicate',
        wrapperClass: 'menu-wrapper',
        slidesPerView: 'auto',
        mousewheelControl : true

    });

    var verticalSwiperNestedSlideContainer = new Swiper('.slide-list' ,{
        mode: 'vertical',
        speed: 200,
        slideElement: 'section',
        slideClass: 'slide-slide',
        slideActiveClass: 'slide-slide-active',
        slideVisibleClass: 'slide-slide-visible',
        slideDuplicateClass: 'slide-slide-duplicate',
        wrapperClass: 'slide-wrapper',
        slidesPerView: 'auto',
        mousewheelControl : true

    });


    var swiperContainerPagination = container.querySelector('.swiper-container-pagination');
    document.querySelector('#stipes_button').addEventListener('touchend', stipesButtonTouch);

    function stipesButtonTouch(e) {
        e.preventDefault();
        e.stopPropagation();
        if ( document.querySelector('.wrapper').children[1].classList.contains('swiper-slide-active') ) {
            swiperContainerPagination.children[0].dispatchEvent(eventYoch);
//            container.querySelector('#stipes_button').classList.remove('current-page');
        } else {
            swiperContainerPagination.children[1].dispatchEvent(eventYoch);
//            document.querySelector('#stipes_button').classList.add('current-page')
        }
    }

    sw_container.addEventListener('touchend', containerTouchend);
    sw_container.addEventListener('touchmove', containerTouchmove);

    function containerTouchend (e) {
        e.preventDefault();
        e.stopPropagation();
        swiperContainerPagination.children[1].dispatchEvent(eventYoch);
    }

    function containerTouchmove(e) {
//        e.preventDefault();
        e.stopPropagation();
    }

    document.querySelector('#drops_button').addEventListener('touchend', stipesDropsTouch);

    function stipesDropsTouch(e) {
        e.preventDefault();
        e.stopPropagation();
        if ( document.querySelector('.wrapper').children[1].classList.contains('swiper-slide-active') ) {
            swiperContainerPagination.children[2].dispatchEvent(eventYoch);
//            container.querySelector('#drops_button').classList.remove('current-page');

        } else {
            swiperContainerPagination.children[1].dispatchEvent(eventYoch);
//            document.querySelector('#drops_button').classList.add('current-page')
        }
    }

    document.addEventListener('showElementsEvent', showElems);
    document.addEventListener('hideElementsEvent', hideElems);

    var elems = document.querySelectorAll('#stipes_button, #drops_button');

    function showElems() {
        for (var i = 0; i < elems.length; i++) {
            elems[i].style.display = 'block';
        }
    }

    function hideElems() {
        for (var i = 0; i < elems.length; i++) {
            elems[i].style.display = 'none';
        }
    }
}