
function AddAudio() {
    function css(elem,css){
        for (var j in css) {
            elem.style[j] = css[j];
        }
    }

    var windowWidth = window.innerWidth;

    return {
        init: function (opts) {
            this.render(opts);
        },
        touchTarget : '',
        audioParam: false,
        globalAudio: '',
        currentAudio: '',
        render: function (opts) {
            var _this = this;

            if ( opts.globalAudio ) {
                _this.globalAudio = _this.addAudio(opts.globalAudio).querySelector('audio');
                var elem = _this.addAudio(opts.globalAudio);

                opts.container ? document.querySelector(opts.container).appendChild(elem) : document.body.appendChild(elem);


            }
            if ( opts.slidesAudio ) {
                opts.slidesAudio.forEach(function(elem) {
                    var container = document.querySelector(elem.container);
                    container.classList.add('audio-container');
                    container.appendChild(_this.addAudio(elem));
                });
            }

            var elems = document.querySelectorAll('*');

            for ( var i = 0; i < elems.length; i++ ) {
                elems[i].addEventListener('slideaudiochange', function(e){
                    _this.touchTarget = this;
                });
            }

            _this.drawSecurityScreen(opts);
            _this.drawAudioButton(opts, _this);


            document.addEventListener('hideElementsEvent', function(){
                _this.musicControls.style.display = 'none';

            });
            document.addEventListener('showElementsEvent', function() {
                _this.musicControls.style.display = 'block';
            });

        },

        addAudio: function(opts) {

            var _this = this;
            var audio = document.createElement('audio'),
                div = document.createElement('div');

            for ( var prop in opts ) {
                if ( typeof opts[prop] == 'object' || typeof opts[prop] == 'array' ) {
                    continue;
                }
                audio.setAttribute( prop, opts[prop]);
            }

            for (var i = 0; i < opts.sources.length; i++) {
                var source = document.createElement('source');
                source.setAttribute('src', opts.sources[i].src);
                source.setAttribute('type', opts.sources[i].type);
                audio.appendChild(source);
            }

            audio.autobuffer = true;
            audio.load();

            div.appendChild(audio);

            var touchEndEvent = document.createEvent('Event');
            touchEndEvent.initEvent('touchend')

            audio.addEventListener('videoEvent', function() {
                if ( _this.audioParam ) {
                    _this.tapArea.dispatchEvent(touchEndEvent);
                }

            });
            return div;
        },

        drawSecurityScreen: function(opts) {
            var _this = this;

            var screen = document.createElement('div');

            screen.classList.add('screen-music-security');

            var img = document.createElement('div'),
                p = document.createElement('div'),
                span1 = document.createElement('span'),
                span2 = document.createElement('span'),
                fragment = document.createDocumentFragment();

            img.style.background = 'url('+opts.playButtonUrl+')';
            img.style.backgroundSize = 'contain';

            span1.innerHTML = opts.silentInnerHTML;
            span2.innerHTML = opts.silenceButtonHTML;

            span1.className = opts.silentInnerClassName;
            span2.className = opts.silenceButtonClassName;

            img.style.position = 'absolute';

            img.style.width  = windowWidth/2 + 'px';
            img.style.height = windowWidth/2 + 'px';
            img.style.left = '50%';
            img.style.top = '50%';
            img.style.marginLeft = - windowWidth/4 +'px';
            img.style.marginTop = - windowWidth/4 +'px';

            p.appendChild(span1);
            p.appendChild(span2);

            screen.appendChild(img);
            screen.appendChild(p);
            fragment.appendChild(screen);
            document.body.appendChild(fragment);

            p.style.position = 'relative';
            p.style.top = img.getBoundingClientRect().bottom+'px';

            screen.addEventListener('touchmove',function(e) {
                e.preventDefault();
                e.stopPropagation();
            });

            img.addEventListener('touchend',function(e) {
                e.preventDefault();
                e.stopPropagation();

                document.body.removeChild(screen);

                _this.audioParam = true;
                _this.audioCurrentCheck(e, _this)

            });
            span2.addEventListener('touchend',function(e) {
                e.preventDefault();
                e.stopPropagation();
                document.body.removeChild(screen);
                _this.audioParam = false;
                _this.audioCurrentCheck(e, _this)

            })
        },

        drawAudioButton: function(opts, _this) {

            var div = document.createElement('div'),
                tapArea = document.createElement('div'),
                span = document.createElement('span'),
                i = document.createElement('i');

            i.className = 'icon-music';
            div.className = opts.audioIcon.className;
            i.appendChild(tapArea);
            div.appendChild(i);
            div.appendChild(span);

            if ( _this.currentAudio ) {
                div.style.display = 'block';
            } else {
                div.style.display = 'none';
            }

            _this.musicButton = i;
            _this.musicState = span;
            _this.musicControls = div;
            _this.tapArea = tapArea;

            opts.container ? document.querySelector(opts.container).appendChild(div) : document.body.appendChild(div);

            _this.notesAnimation = new _this.musicNotesAnimation(opts, _this);

            _this.tapArea.addEventListener('touchend', function(e){

                if ( _this.audioParam ) {
                    _this.audioParam = false;
                    _this.audioCurrentCheck(e, _this);
                    _this.audioShow();
                } else {
                    _this.audioParam = true;
                    _this.audioCurrentCheck(e, _this);
                    _this.audioShow();
                }

            });
        },

        musicNotesAnimation: function(opts, _this){

            var defaults = {
                steams: ['<img src="./img/musicalNotes.png"/>',
                    '<img src="./img/musicalNotes.png"/>',
                    '<img src="./img/musicalNotes.png"/>',
                    '<img src="./img/musicalNotes.png"/>',
                    '<img src="./img/musicalNotes.png"/>',
                    '<img src="./img/musicalNotes.png"/>'],

                steamsFontFamily: ["Verdana", "Geneva", "Comic Sans MS", "MS Serif", "Lucida Sans Unicode", "Times New Roman", "Trebuchet MS", "Arial", "Courier New", "Georgia"],
                steamFlyTime: 3e3,
                steamInterval: 1000,
                steamMaxSize: 30,
                steamHeight: 100,
                steamWidth: 50
            };

            function drawMusicNote(opts, _this) {
                var b = randomizer(8, m.steamMaxSize),
                    c = e(1, m.steamsFontFamily),
                    d = "#" + e(6, "0123456789ABCDEF"),
                    h = randomizer(25, 50),
                    i = randomizer(-90, 89),
                    j = g(.4, 1),
                    l = "-webkit-transform";

                l = l + ":rotate(" + i + "deg) scale(" + j + ");";

                var p = document.createElement('span'),
                    q = randomizer(0, n - m.steamWidth - b);

                p.className = 'coffee-steam z-show';
                p.innerHTML =  e(1, m.steams);

                q > h && (q = randomizer(0, h)),
                    p.style.position = "absolute";
                p.style.left = h+'px';
                p.style.top = m.steamHeight + 'px';
                p.style.fontSize = b + "px";
                p.style.color = d;
                p.style.fontFamily = c;
                p.style.display = "block";
                p.style.opacity = 1;

                p.setAttribute("style", p.getAttribute("style") + l);

                o.appendChild(p);

                var param = randomizer(m.steamHeight , m.steamHeight/2);
                animate({
                    delay: 30,
                    duration: randomizer(m.steamFlyTime / 2, 1.2 * m.steamFlyTime),
                    delta: linear(),
                    step: function(delta) {

                        p.style.top = m.steamHeight - param * ( delta )+'px';
                        p.style.left = h + q * ( 1 - delta )+'px';
                        p.style.opacity = ( 1 - delta );

                        if ( delta == 1 ) {
                            p.parentNode.removeChild(p);
                            p = null;
                        }
                    }
                });
            }

            function animate(opts) {

                var start = new Date;
                var delta = opts.delta || linear;

                var timer = setInterval(function() {
                    var progress = (new Date - start) / opts.duration;

                    if (progress > 1) progress = 1;

                    opts.step( delta(progress) );

                    if (progress == 1) {
                        clearInterval(timer);
                        opts.complete && opts.complete();
                    }
                }, opts.delay || 13);

                return timer;
            }

            function linear(progress) {
                return progress
            }
            function circ(progress) {
                return 1 - Math.sin(Math.acos(progress))
            }
            function quint(progress) {
                return Math.pow(progress, 5)
            }
            function makeEaseOut(delta) {
                return function(progress) {
                    return 1 - delta(1 - progress)
                }
            }
            var h = null,
                j = null,
                bezier = "cubic-bezier(.09,.64,.16,.94)",
                l = this,
                m = defaults,
                n = m.steamWidth,
                o = document.createElement('div');

            o.className = "coffee-steam-box";


            o.style.height = m.steamHeight+'px';

            o.style. width = m.steamWidth+'px';
            o.style.left = 0+'px';
            o.style.top = -90+'px';
            o.style.position = "absolute";
            o.style.overflow = "hidden";
            o.style.zIndex = 0;

            _this.musicControls.appendChild(o);

            function animateContainer() {
                var a = randomizer(-10, 10);

                a += parseInt(o.style.left);

                a >= 10 ? a = 10 : -10 >= a && (a = -10);

                setInterval(function(){
                    o.style.left = a * ( 1 ) +'px';
                    o.style.webkitTransition = 'left '+randomizer(1e3, 3e3)/1000+'s '+bezier+' 0s';
                    o.style.transition = 'left '+randomizer(1e3, 3e3)/1000+'s '+bezier+' 0s';

                }, (randomizer(1e3, 3e3))*2)
            }

            function e(a, b) {
                a = a || 1;
                var c = "", d = b.length - 1, e = 0;
                for (i = 0; a > i; i++)e = randomizer(0, d - 1), c += b.slice(e, e + 1);
                return c
            }

            function randomizer(a, b) {
                var c = b - a, d = a + Math.round(Math.random() * c);
                return parseInt(d)
            }

            function g(a, b) {
                var c = b - a, d = a + Math.random() * c;
                return parseFloat(d)
            }

            return {
                stop : function () {
                    clearInterval(h);
                    clearInterval(j);
                    this.audioAnimation = true;
                },
                audioAnimation: true,
                start : function () {

                    if ( !this.audioAnimation ) return;

                    h = setInterval(function () {
                        drawMusicNote()
                    }, randomizer(m.steamInterval / 2, m.steamInterval))

                    j = setInterval(function () {

                    }, randomizer(100, 1e3) + randomizer(1e3, 3e3))
                    this.audioAnimation = false;

                }
            }

        },

        audioCurrentCheck: function(e, _this) {

            var _this = this;
            if ( _this.touchTarget && _this.touchTarget.querySelector('audio') ) {
                if ( _this.audioParam ) {
                    if (_this.currentAudio) stopMedia(), _this.notesAnimation.stop();

                    _this.touchTarget.querySelector('audio').play();

                    _this.notesAnimation.start();

                    if ( _this.currentAudio ) _this.currentAudio.removeEventListener('play', _this.addPlayClass);
                    if ( _this.currentAudio ) _this.currentAudio.removeEventListener('pause', _this.removePlayClass);

                    _this.currentAudio = _this.touchTarget.querySelector('audio');

                    _this.stopMedia();
                    _this.currentAudio.addEventListener('play', _this.addPlayClass);
                    _this.currentAudio.addEventListener('pause', _this.removePlayClass);
                    _this.musicState.innerHTML = 'PLAY'

                } else {
                    if ( _this.currentAudio ) _this.currentAudio.pause(), _this.notesAnimation.stop();
                    _this.musicState.innerHTML = 'PAUSE';

                }
                _this.musicControls.style.display = 'block';

            } else {

                if ( _this.audioParam ) {
                    if ( _this.globalAudio ) {
                        if ( _this.currentAudio != _this.globalAudio ) {
                            if ( _this.currentAudio ) {
                                _this.musicControls.style.display = 'none';
                                _this.currentAudio.pause();
                                _this.notesAnimation.stop();
                            }
                        }
                        _this.musicState.innerHTML = 'PLAY';
                        if ( _this.globalAudio ) {

                            _this.stopMedia();

                            _this.globalAudio.play();
                            _this.notesAnimation.start();
                            _this.musicControls.style.display = 'block';

                        } else {
                            _this.musicControls.style.display = 'none';
                        }

                    } else {
                        _this.musicState.innerHTML = 'PAUSE';
                        _this.musicControls.style.display = 'block';

                        if ( _this.currentAudio ) {
                            _this.currentAudio.pause();
                            _this.notesAnimation.stop();
                        } else {
                            _this.musicControls.style.display = 'none';

                        }
                        if ( !_this.globalAudio ) {
                            _this.musicControls.style.display = 'none';
                        }
                    }

                    _this.currentAudio = _this.globalAudio;

                    if ( _this.currentAudio ) _this.currentAudio.addEventListener('play', _this.addPlayClass);
                    if ( _this.currentAudio ) _this.currentAudio.addEventListener('pause', _this.removePlayClass);


                } else {

                    _this.musicState.innerHTML = 'PAUSE';
                    if (_this.currentAudio) {
                        _this.currentAudio.pause();
                        _this.notesAnimation.stop();
                        _this.musicControls.style.display = 'block';
                    }
                    if ( _this.globalAudio ) {
                        _this.musicControls.style.display = 'block';
                    } else {
                        _this.musicControls.style.display = 'none';
                    }

                }
            }
        },
        stopMedia: function() {
            var _this = this;
            var mediaElements = document.querySelectorAll('audio, video');

            for (var i = 0; i < mediaElements.length; i++ ) {

                if ( _this.globalAudio != mediaElements[i] || _this.currentAudio != mediaElements[i]) {

                    mediaElements[i].pause();

                }

            }
        },
        addPlayClass: function(){

            document.querySelector('.icon-music').classList.add('play-music');
            document.querySelector('.icon-music').classList.remove('pause-music');
        },
        removePlayClass: function(){

            var _this = this;
            document.querySelector('.icon-music').classList.remove('play-music');
            document.querySelector('.icon-music').classList.add('pause-music');
        },
        audioShow: function(){
            var _this = this;
            _this.musicState.classList.add('audio-show');
            setTimeout(function(){
                _this.musicState.classList.remove('audio-show');
            }, 500)

        }
    }
}
