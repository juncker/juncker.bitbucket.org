function CloudMoving() {
    return {
        init: function(opts) {
            this.render(opts);
        },
        render: function(opts) {
            var container = document.querySelector(opts.container),
                imgContainer = document.createElement('div');

            imgContainer.className = opts.cloudSubstrateClass;
            for ( var i = 0; i < opts.cloudsNumber; i++ ) {
                if ( i > 12 ) break;
                var box  = document.createElement('i');
                imgContainer.appendChild(box);

            }
            container.insertBefore(imgContainer, container.firstChild)
        }
    };
}

