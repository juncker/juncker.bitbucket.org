var CubeSlider = function(options){
    var el = document.createElement('div'),
        transformProps = 'transform WebkitTransform MozTransform OTransform msTransform'.split(' '),
        transformProp = support(transformProps),
        transitionDuration = 'transitionDuration WebkitTransitionDuration MozTransitionDuration OTransitionDuration msTransitionDuration'.split(' '),
        transitionDurationProp = support(transitionDuration)

    document.querySelector(options.container).style.width = screen.width+"px";
    document.querySelector(options.container).style.height = screen.height+"px";
    document.querySelector(options.container).style.webkitPerspective = screen.height*2+"px";
    document.querySelector(options.wrapper).style.width = screen.width+"px";
    document.querySelector(options.wrapper).style.height = screen.height+"px";
    document.querySelector(options.slide).classList.add('active-item');

    switch (options.rotate) {
        case 'horizontal':
            for ( var i=0; i<document.querySelectorAll(options.slide).length; i++ ) {
                document.querySelectorAll(options.slide)[i].style.width = screen.width+"px";
                document.querySelectorAll(options.slide)[i].style.height = screen.height+"px";
                document.querySelectorAll(options.slide)[i].style.webkitTransform = 'rotateX(-'+i*90+'deg) translateZ('+screen.height/2+'px)';
            }
            break;

        case 'vertical':
            for ( var i=0; i<document.querySelectorAll(options.slide).length; i++ ) {
                document.querySelectorAll(options.slide)[i].style.width = screen.width+"px";
                document.querySelectorAll(options.slide)[i].style.height = screen.height+"px";
                document.querySelectorAll(options.slide)[i].style.webkitTransform = 'rotateY('+i*90+'deg) translateZ('+screen.width/2+'px)';
            }
            break;
    }

    function support(props) {
        for(var i = 0, l = props.length; i < l; i++) {
            if(typeof el.style[props[i]] !== "undefined") {
                return props[i];
            }
        }
    }

    var mouse = {
            start : {
                x: 0,
                y: 0
            }
        },
        touch = document.ontouchmove !== undefined,
        result_event,
        viewport = {
            x: 0,
            y: 0,
            el: document.querySelector(options.wrapper),
            move: function(coords) {
                if(coords) {
                    if(typeof coords.x === "number") this.x = coords.x;
                    if(typeof coords.y === "number") this.y = coords.y;
                }

                switch (options.rotate) {
                    case 'horizontal':
                        this.el.style[transformProp] = "rotateX("+this.y+"deg)";
                        break;

                    case 'vertical':
                        this.el.style[transformProp] = "rotateY("+this.x+"deg)";
                        break;
                }
            },
            reset: function() {
                this.move({x: 0, y: 0});
            }
        };

        viewport.duration = function() {
            var d = /*touch ? 50 :*/ 500;
            viewport.el.style[transitionDurationProp] = d + "ms";
            return d;
        }();

        function visibilityOfItems(i) {
            for (var j = 0; j < document.querySelectorAll(options.slide).length; j++) {
                document.querySelectorAll(options.slide)[j].classList.remove('active-item')
            }

            if ( document.querySelectorAll(options.slide)[i-1] ) (document.querySelectorAll(options.slide)[i-1]).classList.add('active-item');
            (document.querySelectorAll(options.slide)[i]).classList.add('active-item');
            if ( document.querySelectorAll(options.slide)[i+1] ) (document.querySelectorAll(options.slide)[i+1]).classList.add('active-item');

        }

        function onMouseMove(event) {
            event.preventDefault();
            event.stopPropagation();

            event.touches ||  event.originalEvent ? event = event.touches[0] : null;

            // Only perform rotation if one touch or mouse (e.g. still scale with pinch and zoom)

            if(!touch || !(event.originalEvent && event.originalEvent.touches.length > 1)) {

                // Get touch coords
                var move_viewport =  document.createEvent("Event");
                move_viewport.initEvent('move-viewport', true, true);

                move_viewport.x = event.pageX;
                move_viewport.y = event.pageY;

//                    event.originalEvent.touches ? event = event.originalEvent.touches[0] : null;
                document.querySelector(options.container).dispatchEvent(move_viewport);

            }
        }

        document.addEventListener('touchstart', function(evt) {
            evt.preventDefault();

            delete mouse.last;

            evt.touches ||  evt.originalEvent ? evt = evt.touches[0] : null;

            mouse.start.x = evt.pageX;
            mouse.start.y = evt.pageY;

            document.addEventListener('touchmove', onMouseMove);
        });

        document.addEventListener('touchend', function () {

            var rotate_expresion;

            switch (options.rotate) {
            case 'horizontal':
                rotate_expresion = /rotateX\(-?\d+/;
                break;

            case 'vertical':
                rotate_expresion = /rotateY\(-?\d+/;
                break;
            }

            if ( (document.querySelector('.wrapper').style.webkitTransform).match(rotate_expresion) == null
                || (document.querySelector('.wrapper').style.webkitTransform).match(rotate_expresion)[0].match(/\d+/) <
                (document.querySelectorAll(options.slide).length-1)*90 ) {
                console.log('otuchend', result_event)
                if (result_event > 0) {

                    switch (options.rotate) {
                        case 'horizontal':
                            viewport.move({
//                    x: viewport.x + parseInt((mouse.start.y - evt.y)/movementScaleFactor),
//                    y: viewport.y + parseInt((mouse.start.y - evt.y)/movementScaleFactor)
                                x: viewport.x + 90,
                                y: viewport.y + 90
                            });
                            break;

                        case 'vertical':
                            viewport.move({
                                x: viewport.x - 90,
                                y: viewport.y - 90
                            });
                            break;
                    }

                    for (var i = 0; i < document.querySelectorAll(options.slide).length; i++) {

                        if ( (document.querySelector('.wrapper').style.webkitTransform).match(rotate_expresion) != null
                            && (document.querySelector('.wrapper').style.webkitTransform).match(rotate_expresion)[0].match(/\d+/)[0] ==
                            document.querySelectorAll(options.slide)[i].style.webkitTransform.match(rotate_expresion)[0].match(/\d+/)[0] )
                        {
                            visibilityOfItems(i);
                            break;
                        }
                    }

                }
            }
            if ( (document.querySelector('.wrapper').style.webkitTransform).match(rotate_expresion) != null &&
                (document.querySelector('.wrapper').style.webkitTransform).match(rotate_expresion)[0].match(/\d+/) > 0 ) {
                console.log('otuchend----', result_event < 0)

                if (result_event < 0) {
                    console.log('otuchend----', result_event < 0)
                    switch (options.rotate) {
                        case 'horizontal':
                            viewport.move({
                                x: viewport.x - 90,
                                y: viewport.y - 90
                            });
                            break;

                        case 'vertical':
                            viewport.move({
                                x: viewport.x + 90,
                                y: viewport.y + 90
                            });
                            break;
                    }

                    for (var i = 0; i < document.querySelectorAll(options.slide).length; i++) {

                        if ((document.querySelector('.wrapper').style.webkitTransform).match(rotate_expresion) != null
                            && (document.querySelector('.wrapper').style.webkitTransform).match(rotate_expresion)[0].match(/\d+/)[0] ==
                            document.querySelectorAll(options.slide)[i].style.webkitTransform.match(rotate_expresion)[0].match(/\d+/)[0])
                        {
                            visibilityOfItems(i);
                            break;
                        }
                    }
                }
            }
            document.removeEventListener('touchmove', onMouseMove);
        });


        document.querySelector(options.container).addEventListener('move-viewport', function(evt) {
            // Reduce movement on touch screens
            var movementScaleFactor = /*touch ? 4 :*/ 1;

            if (!mouse.last) {
                mouse.last = mouse.start;

            } else {
//                if (forward(mouse.start.x, mouse.last.x) != forward(mouse.last.x, evt.x)) {
//                    mouse.start.x = mouse.last.x;
//                }
//                if (forward(mouse.start.y, mouse.last.y) != forward(mouse.last.y,  evt.y)) {
//                    console.log('third')
//
//                    mouse.start.y = mouse.last.y;
//                } else {
////                    console.log('sucks', mouse.start.x, mouse.last.x, mouse.last.x, evt.x)
//                }
            }

            switch (options.rotate) {
                case 'horizontal':
                    result_event = mouse.start.y - evt.y;
                    break;

                case 'vertical':
                    result_event = mouse.start.x - evt.x;
                    break;
            }

            mouse.last.x = evt.x;
            mouse.last.y = evt.y;

            function forward(v1, v2) {
                return v1 >= v2 ? true : false;
            }
        });

};

var cube_slider = new CubeSlider({
    container: '.nav-slide',
    wrapper: '.wrapper',
    slide: '.sw-slide',
    rotate: 'horizontal'
});