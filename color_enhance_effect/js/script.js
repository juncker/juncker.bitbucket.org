function ColorEnhance(opts){

var swipe = document.querySelector('.mengban-swipe')
    swipe.style.background = 'url(./img/swipe_120X120.png) no-repeat';
    swipe.style.opacity = 1;
    swipe.style.marginLeft = '-30px';
    swipe.style.zIndex = 10;
    swipe.style.bottom = '50px';
    swipe.style.left = '50%';
    swipe.style.position = 'fixed';
    swipe.style.width = '120px';
    swipe.style.height = '120px';


    var swap_arr = [];

    for ( var y = 0; y < 7; y++ ) for ( var x = 0; x < 6; x++ ) swap_arr.push('-'+x*121+'px -'+y*120+'px');

    var step = 0,
        cloud_step = 0;

    var swiper_interval = setInterval(function(){
    step++;

    if (step >= swap_arr.length) step = 0;

    swipe.style.backgroundPosition = swap_arr[step];

}, 40);

    var white_mask = document.querySelector('#mask .bg');
    var enhance = false;
    var clickX = new Array();
    var clickY = new Array();

    document.querySelector('.page-con.j-motion').addEventListener('touchstart', function(e){
        enhance = true;
        e.touches ||  e.originalEvent ? e = e.touches[0] : null;
    })

    document.querySelector('.page-con.j-motion').addEventListener('touchmove', function(e){

        e.preventDefault();

        e.touches ||  e.originalEvent ? e = e.touches[0] : null;

        var moveX = e.pageX;
        var moveY = e.pageY;

        if ( enhance ) {
            white_mask.style.opacity-= 0.015;
        }

        if ( white_mask.style.opacity < 0.015 && enhance) {
            enhance = false;

            cloudAnimation();
        }

    })

    var cloud_arr = [];

    for ( var y = 0; y < 5; y++ ) for ( var x = 0; x < 4; x++ ) cloud_arr.push('-'+x*640+'px -'+y*1008+'px');

    var elem = document.querySelector('.m-page.m-fengye');

    elem.style.webkitMaskImage = 'url(./img/clouds_for_MC.png)';
    elem.style.webkitMaskRepeat = 'no-repeat';
    elem.style.webkitMaskSize = '400%';


    function cloudAnimation(){
        document.querySelector('.translate-front').style.opacity = 1;
        var cloud_interval = setInterval(function(){
            cloud_step++;

            if (cloud_step >= cloud_arr.length-2) {
                clearInterval(cloud_interval);
//                document.querySelector('.translate-front').classList.add('z-hide');

                elem.style.webkitMask = 'none';
                return;
            }

//            document.querySelector('.translate-front').style.opacity -=0.2;
            elem.style.webkitMaskPosition = cloud_arr[cloud_step];
/*
            console.log('mask', document.querySelector('.m-page.m-fengye').style.webkitMask )
            console.log('opacity', document.querySelector('.translate-front').style.opacity )
*/
        }, 40);

    }

}

console.log('hello001')

var smudge = new ColorEnhance({
    elem: '#j-mengban canvas',
    smudge_material_src: '../img/cover_bg_03@2x.jpg',
    duration: 3000,
    lineWidth: 70,
    field_area: 0.7
});

