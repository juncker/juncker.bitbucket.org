# Add ObjectRolling

Add your image with current effect

## Note

CSS styles are important to have a good layout.

## How to use

create new instance of class ObjectRolling

Enter parameters in init method of your instance

```
    /*  Create  new  ObjectRolling instance  */

    var rolling_image = new ObjectRolling();

    /*  call init function of AddText instance      */
    rolling_image.init({
        container: '.eighth_slide',                         // name of container where you want to add your photoalbum
        rollingImgSrc: './img/girl.png',                    // source of your image
        linkWrapperClassName: 'link-for-magazine'           // class name to style your image container
    });
/*  this is default styles that I have used in my example, you can use your own   */
```