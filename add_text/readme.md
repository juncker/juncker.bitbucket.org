# Slider

Add text module.
You can add your text with header to the slides or other containers.



## Note

CSS styles are important to have a good layout.

## How to use

create new instance of class AddText

Enter parameters in init method of your instance

```
/*  call init function of AddText instance      */
    text.init({
        container: '.second_slide',         //container name where text will be added
        elemName: 'unique',                 //name of element with your text
        setVector: 'left',                  //appearing vector ( left, right )
        headingTagName: 'p',                //name of element with your text
        driftCourse: 'down',                // drift vector of hidden text ( up, down )
        heading: 'Heading',                 // your heading text
        text: 'lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet lorem ipsum dolor sit apmet ', // your text
        style: {                            //css style for your container
            position: 'absolute',           // if you don't want to brake your styles use "absolute"
            width: '400px',
            margin: '10px',
            top: '30px',
            left: '30px'
        },
        buttonStyle : {                     //css style for your switch button
            left : '20px',
            backgroundImage: 'url(../img/css_sprite01.png)',
            backgroundRepeat: 'no-repeat',
            position: 'absolute',
            bottom: '5px',
            display: 'inline-block',
            width: '34px',
            height: '34px',
            backgroundPosition: '0 -46px'
        }
    });
/*  this is default styles that I have used in my example, you can use your own   */
```