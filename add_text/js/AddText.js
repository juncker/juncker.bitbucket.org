
function AddText() {
    function css(elem,css){
        for (var j in css) {
            elem.style[j] = css[j];
        }
    }

    return {
        init: function (opts, swiper) {

            var _this = this;
            this.render(opts);
            if (swiper) {
                this.addCallback(swiper);
            }

            var els = document.getElementsByTagName('*');
            for(var i=0 ; i<els.length ; i++){
                els[i].addEventListener("addClassToActive", function(e) {
                    _this.addClass(e, _this)
                });
            }
        },

        addClass: function (e, constructor) {
            if (constructor) {
                constructor.closeOpenContainers();
            } else {
                this.closeOpenContainers();

            }
            var elem = e.target;
            if ( elem.classList.contains('slide-active-animation') ) return;
            elem.classList.add('slide-active-animation');
        },

        render: function (opts) {
            var _this = this;

            var div = document.createElement('div'),
                div_heading = document.createElement('div'),
                div_text = document.createElement('div'),
                span = document.createElement('span'),
                touchButton = document.createElement('div'),
                h = document.createElement(opts.headingTagName),
                p = document.createElement('p');

            this.div = div;
            this.div_heading = div_heading;
            this.div_text = div_text;
            this.p = p;
            this.span = span;
            this.touchButton = touchButton;
            this.opts = opts;

            div.classList.add(opts.elemName);
            div.classList.add(opts.setVector);
            h.style.display = 'block';
            h.innerHTML = opts.heading;
            p.innerHTML = opts.text;
            p.style.overflow = 'hidden';

            span.className = 'smallTxt-arrow txt-arrow css_sprite01';

            div_heading.className = 'text-title';
            div_text.className = 'text-detail';
            touchButton.className = 'touch-area';

            div_heading.appendChild(h);
            div_heading.appendChild(span);
            span.appendChild(touchButton);

            div_text.appendChild(p);

            if (opts.heading) div.appendChild(div_heading);
            if (opts.text) div.appendChild(div_text);

            css(div, opts.style);
            css(span, opts.buttonStyle);

            var container = document.querySelector(opts.container);


            container.style.position = 'relative';
            container.appendChild(div);

            touchButton.style.webkitTransformStyle = 'preserve-3d';

            touchButton.style.webkitTransform = 'translate3d(0,0,100px)';

            this.touchButton.addEventListener('touchend', function (e) {

                _this.span.classList.contains('z-toggle') ? _this.hideAnimation(this, _this.opts) : _this.showAnimation(this, _this.opts);
            });

//            h.style.width = h.getBoundingClientRect().width+'px';

            div_heading.style.height = h.getBoundingClientRect().height+'px';


            h.className = opts.headingClassName;

            p.style.width = opts.textWidth+'px';

            this.primaryHeight = div_heading.getBoundingClientRect().height+'px';
            this.primaryWidth = h.getBoundingClientRect().width+'px';
            console.log('this.primaryWidth', this.primaryWidth);
            console.log(this.primaryHeight)
            div.style.height = this.primaryHeight;
            div.style.width = this.primaryWidth;
            div.setAttribute('data-primary-width', div.style.width);
            this.elemsPosition(opts.driftCourse);

        },
        initListeners: function() {
            var _this = this;
                this.touchButton.addEventListener('touchend', function (e) {
                    _this.span.classList.contains('z-toggle') ? _this.hideAnimation(this, _this.opts) : _this.showAnimation(this, _this.opts);
                });
        },
        elemsPosition: function(position){
            switch (position) {
                case 'down':
                    this.p.style.webkitTransform = 'translate3d(0px, -'+ this.p.getBoundingClientRect().height+'px,0)';
                    this.p.style.webkitTransform = 'translate3d(0px, -'+ this.p.getBoundingClientRect().height+'px,0)';
                    this.p.style.paddingTop = 0;

                    this.div_text.style.top = this.div_heading.getBoundingClientRect().height+'px';
                    break;
                case 'up':
                    this.p.style.webkitTransform = 'translate3d(0px, '+ this.p.getBoundingClientRect().height+'px,0)';
                    this.div_text.style.bottom = this.div_heading.getBoundingClientRect().height+'px';
                    this.p.style.paddingBottom = 0;
                    this.div_heading.style.bottom = 0+'px';
                    break;
            }
        },

        hideAnimation: function(elem, opts){

            elem.parentElement.classList.remove('z-toggle');
            this.div_text.classList.remove('action');
            this.div_text.classList.remove('z-show');

            switch (opts.driftCourse) {
                case 'down':
                    this.p.style.webkitTransform = 'translate3d(0px, -'+ this.p.getBoundingClientRect().height+'px,0)';
                    break;
                case 'up':
                    this.p.style.webkitTransform = 'translate3d(0px, '+ this.p.getBoundingClientRect().height+'px,0)';
                    break;
            }
            this.div.style.height = this.div_heading.getBoundingClientRect().height+'px';
            this.div.style.width = this.primaryWidth;
        },

        showAnimation: function(elem){
            elem.parentElement.classList.add('z-toggle');

            this.closeOpenContainers();

            this.div.style.height = this.div_heading.getBoundingClientRect().height + this.p.getBoundingClientRect().height+ 'px';
            this.div.style.width = this.p.getBoundingClientRect().width + 'px';
            this.p.style.webkitTransform = 'translate3d(0px, 0px,0px)';
            this.div_text.classList.add('action');
            this.div_text.classList.add('z-show');
            this.div_text.style.height = this.div_text.querySelector('p').getBoundingClientRect().height+'px';
        },

        closeOpenContainers: function() {
            var action_array = document.querySelectorAll('.action.z-show');

            if (action_array) {
                for (var i = 0; i < action_array.length; i++) {
                    action_array[i].classList.remove('action');
                    action_array[i].classList.remove('z-show');
                    action_array[i].parentElement.querySelector('.smallTxt-arrow').classList.remove('z-toggle');
                    action_array[i].style.height = '0px';
                    action_array[i].parentElement.style.height = this.primaryHeight;
                    action_array[i].parentElement.style.width = action_array[i].parentElement.getAttribute('data-primary-width');
                    console.log(action_array[i].parentElement)
                }
            }
        },

        getHeight: function(elem, opts){
            return (elem.lineHeight == 'normal') ?
                parseInt(elem.fontSize)*1.2*opts.rows+'px' :
                    elem.lineHeight / elem.lineHeight ?
                parseInt(elem.fontSize)*elem.lineHeight*opts.rows+'px' :
                /px$/.test(elem.lineHeight) ?
                    parseInt(elem.lineHeight)*opts.rows+'px' :
                    parseInt(elem.fontSize)*parseInt(elem.lineHeight)/100*opts.rows +'px'
        }

    }

}
