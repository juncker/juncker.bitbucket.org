function Smudge(opts){

    var canvas = document.createElement('canvas'),
        container;

    opts.container ? container = document.querySelector(opts.container) : container = document.body;

    container.appendChild(canvas)

    if (opts.container) {
        var video_elem = container.querySelectorAll('video');
        for ( var i = 0; i < video_elem.length; i++ ) {
            video_elem[i].style.webkitTransform = 'translate3d(0,0,-10000px)';
        }
    }

    canvas.setAttribute('width', window.innerWidth );
    canvas.setAttribute('height', window.innerHeight );
    canvas.style.position = 'absolute';
    canvas.style.left = '0px';
    canvas.style.top = '0px';
    canvas.style.zIndex= 1000;
    canvas.style.marginRight = '1px';

    var img = new Image();
    img.src = opts.smudge_material_src;



    var clickX = new Array();
    var clickY = new Array();
    var clickDrag = new Array();
    var paint;
    var clickSize = new Array();
    var curSize = "normal";
    var image;

    var context = canvas.getContext('2d');

    img.addEventListener('load', function() {
        context.drawImage(img, 0, 0, canvas.width, canvas.height );
    });

    var cancelCombaining = false;

    canvas.addEventListener('touchstart', function(e){

        e.preventDefault();
        e.stopPropagation();

        e.touches ||  e.originalEvent ? e = e.touches[0] : null;

        var mouseX = e.pageX - this.offsetLeft;
        var mouseY = e.pageY - this.offsetTop;

        paint = true;

        addClick(e.pageX /*- this.offsetLeft*/, e.pageY /*- this.offsetTop*/);
        redraw();

        cancelCombaining = true;
    });

    var param = 0,
        trigger = true;

    canvas.addEventListener('touchmove', function(e){
        e.preventDefault();
        e.stopPropagation();

        e.preventDefault();
        param++;
        if (param ) {
            e.touches ||  e.originalEvent ? e = e.touches[0] : null;

            if(paint){

                addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
                redraw();
            }

        }

    });

    canvas.addEventListener('touchend', function(e){

        var alpha_0 = [];
        param = 0;
        e.touches ||  e.originalEvent ? e = e.touches[0] : null;

        var data = context.getImageData(0, 0, canvas.width, canvas.height).data;

        for(var i = 0, n = data.length; i < n; i += 4) {
            if ( data[i + 3] == 0 ) {
                alpha_0.push(data[i + 3])
            }
        }

        if ( data.length/4*opts.field_area < alpha_0.length ) {
            makeTransperent(); // start from hear
        }
        paint = false;
        cancelCombaining = false;

    });

    if (opts.container) {
        document.addEventListener('canvasEvent', canvasListenEvent)
    }

    function canvasListenEvent(){

        if (canvas.getBoundingClientRect().top == 0 && trigger) {
            document.querySelector('.sw-wrapper').style.zIndex = 10000;
        }
    }

    function addClick(x, y, dragging)
    {

        clickX.push(x);
        clickY.push(y);
        clickDrag.push(dragging);
        clickSize.push(curSize);
    }

    function redraw()
    {

        for(var i=0; i < 10; i++) {


            context.beginPath();

            context.globalCompositeOperation = 'destination-out';

            strokeStyle = "rgba(0,0,0,0)";

            context.lineJoin = "round";
            context.lineWidth = opts.lineWidth;

            context.arc(clickX[clickX.length-1], clickY[clickY.length-1], context.lineWidth, 0, 2 * Math.PI, true);

            context.fill();
            context.closePath();

            context.beginPath();

            context.lineWidth = context.lineWidth*2;

            if(clickDrag[i] && i && cancelCombaining){
                context.moveTo(clickX[clickX.length-2], clickY[clickY.length-2]);
            }else{
                context.moveTo(clickX[clickX.length-1], clickY[clickY.length-1]);
            }

            context.lineTo(clickX[clickX.length-1], clickY[clickY.length-1]);
            context.stroke();
            context.lineWidth = context.lineWidth/2;
            context.globalCompositeOperation = 'destination-over';
            context.closePath();
        }
        context.save();

    }

    function makeTransperent() {
        animate({
            delay: 10,
            duration: opts.duration,
            delta: quad(bounce),
            step: function (delta) {
                canvas.style.opacity = 1 - delta ;
                if ( delta == 1 ) {
                    document.querySelector('.sw-wrapper').style.zIndex = 1;

                    if (opts.container) {
                        var video_elem = container.querySelectorAll('video');
                        for ( var i = 0; i < video_elem.length; i++ ) {
                            video_elem[i].style.webkitTransform = 'translate3d(0,0,0px)';
                        }
                    }

                    canvas.style.display = 'none';
                    canvas.parentElement.removeChild(canvas);
                    trigger = false;
                }
            }
        })
    }

    function animate(opts) {

        var start = new Date;
        var delta = opts.delta || linear;

        var timer = setInterval(function() {
            var progress = (new Date - start) / opts.duration;

            if (progress > 1) progress = 1;

            opts.step( delta(progress) );

            if (progress == 1) {
                clearInterval(timer);
                canvas.remove();
                opts.complete && opts.complete();



            }
        }, opts.delay || 13);

        return timer;
    }

// ------------------ Delta ------------------

    function elastic(progress) {
        return Math.pow(2, 10 * (progress-1)) * Math.cos(20*Math.PI*1.5/3*progress)
    }

    function linear(progress) {
        return progress
    }

    function quad(progress) {
        return Math.pow(progress, 2)
    }

    function quint(progress) {
        return Math.pow(progress, 5)
    }

    function circ(progress) {
        return 1 - Math.sin(Math.acos(progress))
    }

    function back(progress) {
        return Math.pow(progress, 2) * ((1.5 + 1) * progress - 1.5)
    }

    function bounce(progress) {
        for(var a = 0, b = 1, result; 1; a += b, b /= 2) {
            if (progress >= (7 - 4 * a) / 11) {
                return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
            }
        }
    }

    function makeEaseInOut(delta) {
        return function(progress) {
            if (progress < .5)
                return delta(2*progress) / 2
            else
                return (2 - delta(2*(1-progress))) / 2
        }
    }

    function makeEaseOut(delta) {
        return function(progress) {
            return 1 - delta(1 - progress)
        }
    }
}
