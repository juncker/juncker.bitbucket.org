
document.querySelector('#toggle').addEventListener('click', menuToggle);

function menuToggle(e) {
    if ( document.querySelector('nav').classList.contains('open') ) {
        document.querySelector('nav').classList.remove('open');

        closeAll(height);

        } else {
        document.querySelector('nav').classList.add('open');
    }
}

var submenus = document.querySelectorAll('.nav-background li a');

for ( var i = 0; i <submenus.length; i++) {
    submenus[i].addEventListener('click', openSubmenu, false)
}

var height = 0;
var li;

function openSubmenu(e) {

    e.preventDefault();
    var elem = this.nextElementSibling;
    li = elem.querySelectorAll('li');

    height = li.length * li[0].getBoundingClientRect().height;


    if ( elem.classList.contains("open") ) {
        elem.classList.remove("open");

        close(elem, height);

    } else {
        closeAll(height);

        elem.classList.add("open");

        open(elem, height);
    }

}

function open(elem, height){
    animate({
        delay: 1,
        duration: 400,
        delta: makeEaseOut(quad)/*quad(bounce)*/,
        step: function (delta) {
            elem.style.height = height*delta+'px';
        }
    })
}

function closeAll (height){
    var elems = document.querySelectorAll('.submenu.open');

    for ( var i = 0; i <elems.length; i++) {
        elems[i].classList.remove("open");
        close(elems[i], height)
    }
}

function close (elem, height) {

    animate({
        delay: 1,
        duration: 200,
        delta: quint(quad),
        step: function (delta) {
            elem.style.height = height-height*delta+'px';
        }
    })
}

function animate(opts) {

    var start = new Date;
    var delta = opts.delta || linear;

    var timer = setInterval(function() {
        var progress = (new Date - start) / opts.duration;

        if (progress > 1) progress = 1;

        opts.step( delta(progress) );

        if (progress == 1) {
            clearInterval(timer);
            opts.complete && opts.complete();
        }
    }, opts.delay || 13);

    return timer;
}

// ------------------ Delta ------------------

function elastic(progress) {
    return Math.pow(2, 10 * (progress-1)) * Math.cos(20*Math.PI*1.5/3*progress)
}

function linear(progress) {
    return progress
}

function quad(progress) {
    return Math.pow(progress, 2)
}

function quint(progress) {
    return Math.pow(progress, 5)
}

function circ(progress) {
    return 1 - Math.sin(Math.acos(progress))
}

function back(progress) {
    return Math.pow(progress, 2) * ((1.5 + 1) * progress - 1.5)
}

function bounce(progress) {
    for(var a = 0, b = 1, result; 1; a += b, b /= 2) {
        if (progress >= (7 - 4 * a) / 11) {
            return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
        }
    }
}

function makeEaseInOut(delta) {
    return function(progress) {
        if (progress < .5)
            return delta(2*progress) / 2
        else
            return (2 - delta(2*(1-progress))) / 2
    }
}

function makeEaseOut(delta) {
    return function(progress) {
        return 1 - delta(1 - progress)
    }
}