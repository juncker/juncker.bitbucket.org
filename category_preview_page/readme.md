# Slider

Custom slider with left preview menu & your custom menu to the right.


## Note

Do not use in your container elements with other classes or element you don't want to slide

## How to use

create new instance of class Slider

Enter parameters in object:
```
var slider = new Slider({                   // create new instance
    container: '.container',                // css selector for container slides container
    slideClass: 'sw-slide',                 // class name of your slides. Without dot
    menuitems: [                            // menu items list that will be situated by right side
        {
            tagName: 'a',                   // tag name of element that will wrap your html default <div>
            innerHTML: 'menu item 1',       // your inner html
            attributes: {}                  // enter any attributes that you need, default - no attributes
        }

    ]
});
```