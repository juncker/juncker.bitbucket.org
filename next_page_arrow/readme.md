# Arrow

Add arrow to page.

Arrow is disappearing when user swipes the screen.



## Note

You can use your CSS styles and images.
Don't use img tag

## How to use

create new instance of class AddArrow

Enter parameters in init method of your instance

```
/*  call init function of AddArrow instance      */

    arrow.init({
        elemName: 'div',                            // tag name of element
        elemStyle: {},                              // styles you can use for element you have created befor
        imageTagName: 'div',                        // tag name for your image except img tag
        imageStyle: {},                             // styles for image if need
        elemClassName: 'arrow',                     // class name of element
        arrowImgSrc:'url(../img/css_sprite01.png)'  // src of image with arrow
    });

/*  this is default styles that I have used in my example, you can use your own   */
```