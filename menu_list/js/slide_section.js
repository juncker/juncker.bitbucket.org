(function(){
    document.querySelector('.container').style.width = screen.width+"px";
//    document.querySelector('.wrapper>section:nth-child(1)').style.width = Math.round(screen.width*0.7)+"px";
    document.querySelector('.wrapper>section:nth-child(1)').style.width = screen.width+"px";
    document.querySelector('.wrapper>section:nth-child(2)').style.width = screen.width*0.7+"px";
//    document.querySelector('.wrapper>section:nth-child(1)').style.height = screen.height+"px";
    document.querySelector('.wrapper>section:nth-child(1)').style.height = screen.height+"px";
    document.querySelector('.wrapper>section:nth-child(2)').style.height = screen.height+"px";
    document.querySelector('.nav-slide .menu-container').style.height = screen.height-(parseFloat(getComputedStyle(document.querySelector('.menu-container')).padding)*2)+"px";

    var horizontalSwiperParent = new Swiper('.swiper-container',{
        mode: 'horizontal',
        speed: 700,
        slideElement: 'div',
        slideClass: 'nav-slide',
        slideActiveClass: 'swiper-slide-active',
        slideVisibleClass: 'swiper-slide-visible',
        slideDuplicateClass: 'swiper-slide-duplicate',
        wrapperClass: 'wrapper',
        slidesPerView: 'auto',
        initialSlide: 0
    });

    var verticalSwiperNestedSwContainer = new Swiper('.sw-container',{
        mode: 'vertical',
        speed: 700,
        slideClass: 'sw-slide',
        slideActiveClass: 'sw-slide-active',
        slideVisibleClass: 'sw-slide-visible',
        slideDuplicateClass: 'sw-slide-duplicate',
        wrapperClass: 'sw-wrapper',
        mousewheelControl : true,
        pagination: '.sw-container-pagination',
        paginationClickable: true

    });

    var verticalSwiperNestedMenuContainer = new Swiper('.menu-container',{
        mode: 'vertical',
        speed: 700,
        slideElement: 'li',
        slideClass: 'menu-slide',
        slideActiveClass: 'menu-slide-active',
        slideVisibleClass: 'menu-slide-visible',
        slideDuplicateClass: 'menu-slide-duplicate',
        wrapperClass: 'menu-wrapper',
        slidesPerView: 'auto',
        mousewheelControl : true

    });


})();